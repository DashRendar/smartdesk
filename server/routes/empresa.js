const express = require('express');
const path = require('path');
var router = express.Router();
const publicPath = path.join(__dirname, '..', 'public');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const secure = require('ssl-express-www'); 
const { Empresa } = require('../data/schema/empresa');

const { tokenChecker, priviligeChecker } = require('../helpers/auth');

router.get('/state',(req, res) => {
    id = req.query.id
    Empresa.findOne({_id:id}).then((doc) => {
       doc.isDeleted = !doc.isDeleted
       doc.save()
       console.log(doc)
       res.send(doc);
   }).catch((e) => {
       console.log(e);
       res.send('invalid');
   });
});

router.get('/all', (req, res) => {

    Empresa.find().lean().then((doc) => {
        res.send(doc);
    }).catch((e) => {
        console.log(e);
        res.send('invalid');
    });
});

router.get('/allx', (req, res) => {
    
        Empresa.find({isDeleted:false}).lean().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    });

router.post('/add', (req, res) => {
    const data = req.body;
    console.log(data)
    Empresa.count({}, function(err, c) {
        let empresa = new Empresa({name:data.nombre,description:data.descripcion});
        empresa.uid = c
        empresa.save().then((doc) => {
            res.send("Empresa creada!")
        })
    })

});

router.post('/find',(req, res) => {
    dato = req.body.dato
    console.log(dato)
    Empresa.find({ $or:[{name:{$regex:'.*'+dato+'.*','$options': 'i'}},{description:{$regex:'.*'+dato+'.*','$options': 'i'}}]}).lean().then((doc) => {
            res.send(doc);
            console.log(doc)
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    
});
module.exports = router
