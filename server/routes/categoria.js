const express = require('express');
const path = require('path');
var router = express.Router();
const publicPath = path.join(__dirname, '..', 'public');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const secure = require('ssl-express-www'); 
const { Categoria } = require('../data/schema/categoria');
const { Tipo } = require('../data/schema/tipo');

const { tokenChecker, priviligeChecker } = require('../helpers/auth');

router.get('/state',(req, res) => {
    id = req.query.id
    Categoria.findOne({_id:id}).then((doc) => {
       doc.isDeleted = !doc.isDeleted
       doc.save()
       console.log(doc)
       res.send(doc);
   }).catch((e) => {
       console.log(e);
       res.send('invalid');
   });
});


router.get('/all', (req, res) => {
    let response = []
    Categoria.find().populate("tipos").exec().then((doc) => {
        doc.map(function(cat){
            response.push(cat.toJSON({virtuals: true}));
        })
        res.send(response);
    }).catch((e) => {
        console.log(e);
        res.send('invalid');
    });
});



router.post('/find',(req, res) => {
    let response = []
    dato = req.body.dato
    Categoria.find({ $or:[{name:{$regex:'.*'+dato+'.*','$options': 'i'}},{description:{$regex:'.*'+dato+'.*','$options': 'i'}}]}).populate("tipos").exec().then((doc) => {
        doc.map(function(cat){
            response.push(cat.toJSON({virtuals: true}));
        })
        res.send(response);
    }).catch((e) => {
        console.log(e);
        res.send('invalid');
    });
});

router.get('/tipos/all', (req, res) => {
    let dict = []
    Tipo.find({isDeleted:false}).populate("category").exec().then((doc) => {
        doc.forEach(function(tipo){
            if(tipo.category.isDeleted==false){
                dict.push(tipo)
            }
        })
        res.send(dict);
    }).catch((e) => {
        console.log(e);
        res.send('invalid');
    });
});

router.get('/allx', (req, res) => {
    Categoria.find({isDeleted:false}).lean().then((doc) => {
        res.send(doc);
    }).catch((e) => {
        console.log(e);
        res.send('invalid');
    });
});

router.post('/rmtipo',(req, res) => {
    id = req.body.data
    Tipo.findById(id).then((doc) => {
       doc.isDeleted = !doc.isDeleted
       doc.save()
       res.send("Cambio de estado!");
   }).catch((e) => {
       console.log(e);
       res.send('Oh! Oh! Hubo un error');
   });
});

router.post('/addtipo',(req, res) => {
    const data = req.body;
    console.log(data)
    Tipo.count({}, function(err, c) {
        let tipo = new Tipo(data);
        tipo.uid = c
        tipo.save().then((doc) => {
            res.send("Tipo creado!")
        })
    })
});

router.post('/add', (req, res) => {
    const data = req.body;
    console.log(data)
    Categoria.count({}, function(err, c) {
        let empresa = new Categoria({name:data.nombre,description:data.descripcion});
        empresa.uid = c
        empresa.save().then((doc) => {
            res.send("Categoria creada!")
        })
    })
});


module.exports = router
