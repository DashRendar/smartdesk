const express = require('express');
const path = require('path');
var router = express.Router();
const publicPath = path.join(__dirname, '..', 'public');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const secure = require('ssl-express-www'); 
const { Ticket } = require('../data/schema/ticket');
const { Tipo } = require('../data/schema/tipo');
const { Categoria } = require('../data/schema/categoria');
const { Empresa } = require('../data/schema/empresa');
const { User } = require('../data/schema/User');

const { tokenChecker, priviligeChecker } = require('../helpers/auth');


router.post('/add', (req, res) => {
    const data = req.body;
    data.apertura = new Date()
    Tipo.findById(data.tipo).populate('category').exec().then((doc) => {
        User.findById(data.inquilino).populate('inquilino.empresa').exec().then((docx) => {
            console.log(doc)
            console.log(docx)
            const ticket = new Ticket({inquilino:data.inquilino,inquilino_uid:docx.uid,description:data.descripcion,tipo:data.tipo,tipo_uid:doc.uid,apertura:data.apertura, category:doc.category._id, empresa:docx.inquilino.empresa._id, category_uid:doc.category.uid, empresa_uid:docx.inquilino.empresa.uid });
            ticket.save().then((x) => {
                res.send("Ticket creado!")
            })
        })
    })
});

function estadisticaTecnicos(req, res){
    
    }
    
router.post('/data', (req, res) => {
    const inicio = new Date(req.body.inicio);
    const fin = new Date(req.body.fin);
    fin.setDate(fin.getDate() + 1);
    data = {}
    if(inicio>fin){
        console.log("invalid")
        res.send("444")
    }else{
        Ticket.aggregate(
            [
                { "$match": {"apertura":{$lte:fin,$gte:inicio},"estado":{$gte:3}}},
                { "$group": { 
                    "_id": "$tecnico",
                    "total": {"$sum": 1}
                },
            },
            ],
            function(err,results) {
                User.populate( results, { "path": "_id" }, function(err,resulting) {
                    data.tecnicos=resulting
                    //inicia segundo query
                    Ticket.aggregate(
                        [
                            { "$match": {"apertura":{$lte:fin,$gte:inicio}}},
                            { "$group": { 
                                "_id": "$inquilino",
                                "total": {"$sum": 1}
                            },
                        },
                        ],
                        function(err,results2) {
                            User.populate( results2, { "path": "_id" }, function(err,resulting2) {
                                data.inquilinos=resulting2
                                 //inicia tercer query
                                Ticket.aggregate(
                                    [
                                        { "$match": {"apertura":{$lte:fin,$gte:inicio}}},
                                        { "$group": { 
                                            "_id": "$tipo",
                                            "total": {"$sum": 1}
                                        },
                                    },
                                    ],
                                    function(err,results3) {
                                        Tipo.populate( results3, { "path": "_id" }, function(err,resulting3) {
                                            data.tipos=resulting3
                                            //inicia cuarto query
                                            Ticket.aggregate(
                                                [
                                                    { "$match": {"apertura":{$lte:fin,$gte:inicio}}},
                                                    { "$group": { 
                                                        "_id": "$category",
                                                        "total": {"$sum": 1}
                                                    },
                                                },
                                                ],
                                                function(err,results4) {
                                                    Categoria.populate( results4, { "path": "_id" }, function(err,resulting4) {
                                                        data.categorias=resulting4
                                                        //inicia quinto query
                                                        Ticket.aggregate(
                                                            [
                                                                { "$match": {"apertura":{$lte:fin,$gte:inicio}}},
                                                                { "$group": { 
                                                                    "_id": "$empresa",
                                                                    "total": {"$sum": 1}
                                                                },
                                                            },
                                                            ],
                                                            function(err,results5) {
                                                                Empresa.populate( results5, { "path": "_id" }, function(err,resulting5) {
                                                                    data.empresas=resulting5
                                                                    console.log(resulting5)
                                                                    res.send(data)
                                                                });
                                                            }
                                                        )
                                                    });
                                                }
                                            )
                                        });
                                    }
                                )
                            });
                        }
                    )
                });
            }
        )
    }
});



router.get('/abiertos', tokenChecker, (req, res) => {
    const admin = req.userInfo.access.isAdmin;
    const staff = req.userInfo.access.isStaff;
    const uid = req.userInfo.uid;
    if(admin){
        Ticket.find({estado:{$lte:2}}).sort({estado: 1, prioridad:1}).populate('inquilino').populate('tipo').populate('tecnico').
        exec().then((doc) => {
            console.log(doc);
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }else if(staff){
        Ticket.find({estado:{$lte:2},tecnico:uid}).sort({prioridad: 1}).populate('inquilino').populate('tipo').populate('tecnico').exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }else{
        Ticket.find({estado:{$lte:2},inquilino:uid}).sort({estado: 1}).populate('inquilino').populate('tipo').populate('tecnico').exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }
});

router.get('/cerrados', tokenChecker, (req, res) => {
    const admin = req.userInfo.access.isAdmin;
    const staff = req.userInfo.access.isStaff;
    const uid = req.userInfo.uid;
    if(admin){
        Ticket.find({estado:3}).sort({apertura: -1}).populate('inquilino').populate('tipo').populate('tecnico').
        exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }else if(staff){
        Ticket.find({estado:3,tecnico:uid}).sort({apertura: -1}).populate('inquilino').populate('tipo').populate('tecnico').exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }else{
        Ticket.find({estado:3,inquilino:uid}).sort({apertura: -1}).populate('inquilino').populate('tipo').populate('tecnico').exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }
});


router.post('/assign', (req, res) => {
    const data = req.body;
    Ticket.findById(data.id).then((doc) => {
        doc.estado=2
        doc.prioridad=data.prioridad
        User.findById(data.tecnico).then((docx) => {
            doc.tecnico=data.tecnico
            doc.tecnico_uid=docx.uid
            doc.asignado=new Date()
            doc.save()
            res.send("Tecnico Asignado!");
        })
    }).catch((e) => {
        console.log(e);
        res.send('invalid');
    });
});

router.get('/administrativos', tokenChecker, (req, res) => {
    const uid = req.userInfo.uid;
        Ticket.find({estado:4}).sort({estado: 1}).populate('inquilino').populate('tipo').populate('tecnico').
        exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
});


router.get('/state',(req, res) => {
    id = req.query.id
    Ticket.findOne({_id:id}).then((doc) => {
       if(doc.estado<3||doc.estado==4){
           doc.estado=3
           doc.cierre= new Date()
           doc.tiempo_cierre=doc.cierre-doc.asignado
           doc.tiempo_abierto=doc.cierre    -doc.apertura
       }else if(doc.tecnico){
            doc.estado=1
            doc.cierre=""
        }else{
           doc.estado=2
           doc.cierre=""
        }
       doc.save()
       res.send(doc);
   }).catch((e) => {
       console.log(e);
       res.send('invalid');
   });
});

router.get('/state/admin',(req, res) => {
    id = req.query.id
    Ticket.findOne({_id:id}).then((doc) => {
           doc.estado=4
           doc.cierre= new Date()
           doc.save()
       res.send(doc);
   }).catch((e) => {
       console.log(e);
       res.send('invalid');
   });
});
module.exports = router
