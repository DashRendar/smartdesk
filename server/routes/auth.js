const express = require('express');
const path = require('path');
var router = express.Router();
const publicPath = path.join(__dirname, '..', 'public');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const secure = require('ssl-express-www'); 
const { User } = require('../data/db');
const { tokenChecker, priviligeChecker } = require('../helpers/auth');
const nodemailer = require('nodemailer');
const randomstring = require("randomstring");

router.use('/user/check_token', (req, res) => {
    const token = req.header('x-auth');
    if(!token || token === '' || token === 'undefined'){
        res.status(403).send('invalid');
        return;
    }
    const data = jwt.decode(token);
    if(data){
        User.findById(data.uid).then((userData) => {
            if(userData.tokens.indexOf(token) !== -1){
                res.send('success');
            }else{
                res.status(403).send('invalid');
            }
        }).catch((e) => {
            res.status(403).send('invalid');
        });
    }else{
        res.status(403).send('invalid');
    }
});
router.use('/user/login', (req, res) => {
    const data = _.pick(req.body, ['email', 'password']);
    User.findByCredentials(data.email, data.password).then((userData) => {
        if(!userData.privilige[0].isActivate){
            console.log(userData)
            res.status(400).send('invalid');
        }
        userData.generateAuthToken().then((token) => {
	    const {status, uid, name, email, access, iat} = jwt.decode(token);
	    payload = {status, uid, name, email, access, iat};
            res.header('x-auth', token).send(payload);
        });
    }).catch((e) => {
        res.status(400).send('invalid');
    });
});
router.use('/user/logout', tokenChecker, (req, res) => {
    const uid = req.userInfo.uid;
    const currentUsrJwt = req.token;
    User.findById(uid).then((userData) => {
        userData.removeAuthToken(currentUsrJwt).then((response) => {
            if(response.tokens.indexOf(currentUsrJwt) === -1){
                res.send('success');
            }
        });
    }).catch((e) => {
        res.status(401).send('error');
    });
});
router.use('/admin/create_user', tokenChecker, (req, res) => {
    const allowAccess = priviligeChecker(req.userInfo.access, ['isAdmin']);
    if(!allowAccess){
        res.status(403).send('invalid');
        return; 
    }
    let password = randomstring.generate(7);
    req.body.password = password;
    
    if(req.body.privilige.isStaff || req.body.privilige.isAdmin){
        req.body.inquilino = {}
    }
    if(!req.body.privilige.isStaff){
        req.body.tecnico = {}
    }
    User.count({}, function(err, c) {
        delete req.body.items;
        const data = req.body;
        console.log(data)
        const user = new User(data);
        user.uid = c
        user.save().then((doc) => {
            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                  user: 'mario.lopez.cuevas@gmail.com',
                  pass: 'estomecaemal'
                }
              });
              
              var mailOptions = {
                from: 'mario.lopez.cuevas@gmail.com',
                to: req.body.email,
                subject: 'Correo de creacion de usuario',
                html: '<h1>Bienvenido al sistema Smart Desk</h1> <h2>Este sistema es para control de mantenimiento a traves de tickets</h2> <p>usuario:'+req.body.email+'</p><p>Contraseña: '+req.body.password+'</p>'
              };
              
              transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                  console.log(error);
                } else {
                  console.log('Email sent: ' + info.response);
                }
              });
            res.send('success');
        }).catch((e) => {
            console.log(e);
            if(e.code==11000){
                res.send('Correo ya existe Invalido');    
            }
            res.send('invalid');
        });
   });
    
});

router.get('/admin/tecnicos',(req, res) => {
 
    User.find({'privilige.isStaff':true}).populate('tecnico.categoria').
    exec().then((doc) => {
        res.send(doc);
    }).catch((e) => {
        console.log(e);
        res.send('invalid');
    });
});

router.post('/admin/tecnicos',(req, res) => {
        dato = req.body.dato
        cate = req.body.cats
        if(cate=="*"||cate==""){
            console.log("llego")
            User.find({ $and:[{'privilige.isStaff':true},{$or:[{name:{$regex:'.*'+dato+'.*','$options': 'i'}},{'tecnico.cargo':{$regex:'.*'+dato+'.*','$options': 'i'}},{email:{$regex:'.*'+dato+'.*','$options': 'i'}}]}]}).populate('tecnico.categoria').
            exec().then((doc) => {
                res.send(doc);
            }).catch((e) => {
                console.log(e);
                res.send('invalid');
            });
        }else{
        User.find({ $and:[{'privilige.isStaff':true},{'tecnico.categoria':cate},{$or:[{name:{$regex:'.*'+dato+'.*','$options': 'i'}},{'tecnico.cargo':{$regex:'.*'+dato+'.*','$options': 'i'}},{email:{$regex:'.*'+dato+'.*','$options': 'i'}}]}]}).populate('tecnico.categoria').
        exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }
   });

router.get('/admin/inquilinos',(req, res) => {
       User.find({'privilige.isInquilino':true}).populate('inquilino.empresa').
       exec().then((doc) => {
           res.send(doc);
       }).catch((e) => {
           console.log(e);
           res.send('invalid');
       });
   });

router.post('/admin/inquilinos',(req, res) => {
    dato = req.body.dato
    empresa = req.body.empresa
    console.log(req.body)
    if(empresa=="*"||empresa==""){
        console.log("llego")
        User.find({ $and:[{'privilige.isInquilino':true},{$or:[{name:{$regex:'.*'+dato+'.*','$options': 'i'}},{'inquilino.local':{$regex:'.*'+dato+'.*','$options': 'i'}},{email:{$regex:'.*'+dato+'.*','$options': 'i'}}]}]}).populate('inquilino.empresa').
        exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }else{
        User.find({ $and:[{'privilige.isInquilino':true},{'inquilino.empresa':empresa},{$or:[{name:{$regex:'.*'+dato+'.*','$options': 'i'}},{'inquilino.local':{$regex:'.*'+dato+'.*','$options': 'i'}},{email:{$regex:'.*'+dato+'.*','$options': 'i'}}]}]}).populate('inquilino.empresa').
        exec().then((doc) => {
                console.log(doc);
                res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }
});

router.get('/admin/inquilinos/active',(req, res) => {
        User.find({ 'privilige.isInquilino':true, 'privilige.isActivate':true}).populate('inquilino.empresa').
        exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
});


router.get('/admin/tecnicos/active',(req, res) => {
    User.find({ 'privilige.isStaff':true, 'privilige.isActivate':true}).populate('tecnico.categoria').
    exec().then((doc) => {
        res.send(doc);
    }).catch((e) => {
        console.log(e);
        res.send('invalid');
    });
});

router.get('/admin/administradores',(req, res) => {
    
       User.find({'privilige.isAdmin':true}).lean().then((doc) => {
           res.send(doc);
       }).catch((e) => {
           console.log(e);
           res.send('invalid');
       });
   });

   router.post('/admin/administradores',(req, res) => {
    dato = req.body.dato
        User.find({ $and:[{'privilige.isAdmin':true},{$or:[{name:{$regex:'.*'+dato+'.*','$options': 'i'}},{email:{$regex:'.*'+dato+'.*','$options': 'i'}}]}]}).lean().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    
});

router.get('/admin/usuarios/state',(req, res) => {
        id = req.query.id
       User.findOne({_id:id}).then((doc) => {
           doc.privilige[0].isActivate = !doc.privilige[0].isActivate
           doc.save()
           console.log(doc)
           res.send(doc);
       }).catch((e) => {
           console.log(e);
           res.send('invalid');
       });
   });

   router.get('/admin/data', tokenChecker,(req, res) => {
    const uid = req.userInfo.uid;
    console.log(req.userInfo)
    if(req.userInfo.access.isStaff){
        User.findById(uid).populate('tecnico.categoria').
        exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }else if(req.userInfo.access.isInquilino){
        User.findById(uid).populate('inquilino.empresa').
        exec().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }else{
        User.findById(uid).lean().then((doc) => {
            res.send(doc);
        }).catch((e) => {
            console.log(e);
            res.send('invalid');
        });
    }
       
   });

   router.post('/admin/passChange', tokenChecker,(req, res) => {
    let dato = req.body.pass
    const uid = req.userInfo.uid;
       User.findById(uid).then((doc) => {
           doc.password = dato;
           doc.save()
           res.send("Password cambiado con exito!");
       }).catch((e) => {
           console.log(e);
           res.send('Error');
       });
   });


   router.post('/admin/Contacto', tokenChecker,(req, res) => {
    let dato = req.body.descripcion
    let num = req.body.numero
    let id = req.body.id
       User.findById(id).then((doc) => {
           doc.contactos.push({descripcion:dato, numero:num})
           doc.save()
           res.send("Contacto Agregado");
       }).catch((e) => {
           console.log(e);
           res.send('Error');
       });
   });

   router.post('/admin/RmContacto', tokenChecker,(req, res) => {
    let data = req.body.data
    let vector = data.split("|")
    let id = vector[1]
    let num = vector[0]
    let desc = vector[2]
    let idd = vector[3]
       User.findById(id).then((doc) => {
           let index = doc.contactos.findIndex(x => x.id === idd)
           console.log(index)
           if (index !== -1) {
            doc.contactos.splice(index, 1);
            }
           doc.save()
           res.send("Contacto Eliminado");
       }).catch((e) => {
           console.log(e);
           res.send('Error');
       });
   });
   
module.exports = router