const dotenv = require('dotenv');
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if (process.env.NODE_ENV === 'test') {
  dotenv.config({ path: '.env.test' });
} else if (process.env.NODE_ENV === 'development') {
  dotenv.config({ path: '.env.development' });
}
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const secure = require('ssl-express-www'); 
const { User } = require('./data/db');
const { tokenChecker, priviligeChecker } = require('./helpers/auth');

const app = express();
const publicPath = path.join(__dirname, '..', 'public');
const port = process.env.PORT || 3000;

var auth = require('./routes/auth');
var empresa = require('./routes/empresa');
var categoria = require('./routes/categoria');
var ticket = require('./routes/ticket');


if(process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'development'){
    app.use(secure);
}

app.use(express.static(publicPath));
app.use(bodyParser.json()); 

app.use('/',auth);
app.use('/empresa',empresa);
app.use('/tickets',ticket);
app.use('/categoria',categoria);

app.use('*', (req, res) => {
  res.sendFile(path.join(publicPath, 'index.html'));
});

app.listen(port, () => {
  const msg = `Server is up! Listening to the port: ${port}, in env: ${process.env.NODE_ENV}`;
  console.log(msg);
});

module.exports.app = app;
