const mongoose = require('mongoose');

//mongoose Schema object.
const tipoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String,
        required: true,
        trim: true,
    },       
    uid:{
        type:Number
    },
    isDeleted:{
        type: Boolean,
        required: true,
        default: false
    },
    category : { type: mongoose.Schema.Types.ObjectId, ref: 'Categoria' }                
    
}); 


const Tipo = mongoose.model('Tipo', tipoSchema);
module.exports = { Tipo }
