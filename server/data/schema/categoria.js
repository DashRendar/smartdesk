const mongoose = require('mongoose');
const { Tipo } = require('./tipo');

//mongoose Schema object.
const CatSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String,
        required: true,
        trim: true,
    },       
    uid:{
        type:Number
    },
    isDeleted:{
        type: Boolean,
        required: true,
        default: false
    }            
}); 

CatSchema.virtual('tipos', {
    ref: 'Tipo',
    localField: '_id',
    foreignField: 'category'
  });

const Categoria = mongoose.model('Categoria', CatSchema);
module.exports = { Categoria }
