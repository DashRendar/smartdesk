const mongoose = require('mongoose');

//mongoose Schema object.
const ticketSchema = new mongoose.Schema({
    apertura: {
        type: Date,
        required: true,
    },
    cierre: {
        type: Date,
    },
    asignado: {
        type: Date,
    },
    estado: {
        type: Number,
        required: true,
        default:1
    },
    prioridad: {
        type: Number,
        required: true,
        default: 0
    },
    tiempo_cierre: {
        type: Number,
    },
    tiempo_abierto: {
        type: Number,
    },
    description: {
        type: String,
        required: true,
        trim: true,
    },      
    tipo : { type: mongoose.Schema.Types.ObjectId, ref: 'Tipo' },                
    inquilino : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },                
    tecnico : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },                
    category : { type: mongoose.Schema.Types.ObjectId, ref: 'Categoria' },              
    empresa : { type: mongoose.Schema.Types.ObjectId, ref: 'Empresa' },   
    tipo_uid : { type: Number },                
    inquilino_uid : { type: Number },                
    tecnico_uid : { type: Number },                
    category_uid : { type: Number },              
    empresa_uid : { type: Number },               
    
}); 


const Ticket = mongoose.model('Ticket', ticketSchema);
module.exports = { Ticket }
