const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { isEmail } = require('validator');

const TOKEN_SALT = JSON.stringify(process.env.TOKEN_SALT);

//mongoose Schema object.
const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 6,
        trim: true
    },
    uid:{
        type:Number
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true, 
        validate: [ isEmail, 'invalid email' ]   
    },                   
    password: {          
        type: String,    
        required: true,
        trim: true,      
        minlength: 6       
    },                   
    privilige: [{
        isActivate: {
            type: Boolean,
            default: false
        },
        isAdmin: {
            type: Boolean,
            default: false
        },
        isInquilino: {
            type: Boolean,
            default: false
        },
        isStaff: {
            type: Boolean,
            default: false
        }
    }],
    tecnico:{
        categoria : { type: mongoose.Schema.Types.ObjectId, ref: 'Categoria' },                
        cargo: {
            type: String,
            trim: true
        }       
    },
    inquilino:{
        empresa : { type: mongoose.Schema.Types.ObjectId, ref: 'Empresa' },                
        local: {
            type: String,
        }       
    },
    tokens: [{
        type: String,
        required: true
    }],
    contactos: [{
        descripcion: {
            type: String,
        },
        numero: {
            type: String,
        },
    }],
}); 

UserSchema.methods.generateAuthToken = function(){
    const user = this;
    const tokenLimit = 3;
    if(user.tokens.length >= tokenLimit){
        user.tokens.shift(); 
    }
    const access = user.privilige.toObject()[0];
    const token = jwt.sign({uid: user._id.toHexString(), name: user.name, email: user.email, access}, TOKEN_SALT).toString();
    user.tokens = user.tokens.concat([token]); 
    return user.save().then(() => {
        return token;
    });
}; 
UserSchema.methods.removeAuthToken = function(token){
    const user = this;
    if(token){
        try{
            const targetIndex = user.tokens.indexOf(token);
            if(targetIndex !== -1){
                user.tokens = user.tokens.filter( (elm, index) => index !== targetIndex );
                return user.save();
            }
        }catch(e){
            return Promise.reject();
        }
    }else{
        try{
            if(user.tokens.length > 0){
                user.tokens = [];
                return user.save();
            }
        }catch(e){
            return Promise.reject();
        }
    }
};

UserSchema.pre('save', function(next){
    const user = this;
    if(user.isModified('password')){
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();
            });
        });
    }else{
        next();
    }
});

UserSchema.statics.findByCredentials = function(email, password){
    let user = this;
    return user.findOne({email}).then((userData) => {
        if(!userData){
            return Promise.reject();
        }
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, userData.password, (err, res) => {
                if(res){
                    resolve(userData);
                }else{
                    reject();
                }
            });
        });
    });
}

const User = mongoose.model('User', UserSchema);
module.exports = { User }
