const mongoose = require('mongoose');

//mongoose Schema object.
const empresaSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String,
        required: true,
        trim: true,
    },   
    uid:{
        type:Number
    },    
    isDeleted:{
        type: Boolean,
        required: true,
        default: false
    }     
    
}); 


const Empresa = mongoose.model('Empresa', empresaSchema);
module.exports = { Empresa }
