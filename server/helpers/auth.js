const jwt = require('jsonwebtoken');
const { User } = require('../data/db');

const tokenChecker = (req, res, next) => {
    const token = req.header('x-auth');
    if(!token || token === '' || token === 'undefined'){
        res.status(403).send('invalid');
        return; 
    }
    const data = jwt.decode(token);
    if(data){
        User.findById(data.uid).then((userData) => {
            if(userData.tokens.indexOf(token) !== -1){
                req.userInfo = data;
                req.token = token;
                next();
            }else{
                return Promise.reject();
            }
        }).catch((e) => {
            res.status(403).send('invalid');
        });
    }else{
        res.status(403).send('invalid');
    }
};

const priviligeChecker = (access, require) => {
    if(access){
        const len = require.length;
        for(let i = 0; i < len; i++){
            if(access[require[i]] === false)
                return false;
        }
    }else{
        return false;
    }

    return true;
};

module.exports = { tokenChecker, priviligeChecker };
