import React from 'react';
import { Router, Route, Switch, Link, NavLink } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import NotFoundPage from '../components/NotFoundPage';
import LoginPage from '../components/LoginPage';
import DashboardPage from '../components/DashboardPage';
import CreatePage from '../components/CreatePage';
import TecnicosPage from '../components/TecnicosPage';
import InquilinoPage from '../components/InquilinoPage';
import AdminPage from '../components/AdminPage';
import AdminTicket from '../components/AdminTicket';
import TecnicoRegresion from '../components/TecnicoRegresion';
import ChangePass from '../components/ChangePass';
import Empresa from '../components/Empresa';
import Categoria from '../components/Categoria';
import Ticket from '../components/Tickets';
import TicketCerrados from '../components/CloseTickets';
import NotAllowPage from '../components/NotAllowPage';
import Navbar from '../components/Navbar';
import EstadisticaPage from '../components/EstadisticaPage';
import { Widget, addResponseMessage  } from 'react-chat-widget';
export const history = createHistory();
import 'react-chat-widget/lib/styles.css';
import axios from 'axios';

const handleNewUserMessage = async (newMessage) => {
  let response = await axios.get('http://localhost:3002/bot/'+newMessage)
  addResponseMessage(response.data.message)
}

const AppRouter = () => (
  
  <Router history={history}>
    <div>
      <Switch>
        <Route path="/" component={LoginPage} exact={true} />
        <Route path="/tiquetes/cerrados" component={TicketCerrados} />
        <Route path="/tiquetes/administrativos" component={AdminTicket} />
        <Route path="/tiquetes" component={Ticket} />
        <Route path="/regresion/tecnico" component={TecnicoRegresion} />
        <Route path="/analisis" component={EstadisticaPage} />
        <Route path="/categorias" component={Categoria} />
        <Route path="/dashboard" component={Ticket} />
        <Route path="/admin/createusr" component={CreatePage} />
        <Route path="/tecnicos" component={TecnicosPage} />
        <Route path="/administradores" component={AdminPage} />
        <Route path="/perfil" component={ChangePass} />
        <Route path="/inquilinos" component={InquilinoPage} />
        <Route path="/empresas" component={Empresa} />
        <Route path="/notAllow" component={NotAllowPage} />
        <Route component={NotFoundPage} />
      </Switch>
      <Widget
          handleNewUserMessage={handleNewUserMessage}
          title="SmartDesk Chat"
          subtitle=""
        />
    </div>
  </Router>
);

export default AppRouter;
