import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import Navbar from './Navbar';
import AuthChecker from '../routers/AuthChecker';
import M from 'materialize-css';
import axios from 'axios';
import { getStorage } from '../helpers/cookie';
import {Bar,Doughnut,Line} from 'react-chartjs-2';
export class TecnicosPage extends React.Component{
    constructor(props){       
        super(props);
    }
    state = {
        inicio: "",
        fin:"",
        tipos:[],
        tecnicos:[],
        inquilinos:[],
        data:{},
        tecnicosNameData: [],
        tecnicosDataData: [],
        empresasNameData: [],
        empresasDataData: [],
        categoriasNameData: [],
        categoriasDataData: [],
        tiposNameData: [],
        tiposDataData: [],
        inquilinosNameData: [],
        inquilinosDataData: []
    };
    onInicioChange = (e) => {
        let local = e.target.value;
        this.setState({inicio:local});
    };
    onFinChange = (e) => {
        let dato = e.target.value;
        this.setState({fin:dato});
    };
    onSubmit = (e) => {
        e.preventDefault();
        if(this.state.inicio=="" || this.state.fin==""){
            M.toast({html: "Seleccione las fechas de inicio y fin"})
        }else{
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/tickets/data', {"inicio":this.state.inicio,"fin":this.state.fin}).then((res) => {
                if(res.data=="444"){
                    M.toast({html: "La fecha de inicio no puede ser despues de la fecha del fin"})
                }else{
                    this.setState({tecnicos:res.data.tecnicos});
                    this.setState({empresas:res.data.empresas});
                    this.setState({categorias:res.data.categorias});
                    this.setState({inquilinos:res.data.inquilinos});
                    this.setState({tipos:res.data.tipos});
                    this.setState({tiposDataData:this.tiposData()});
                    this.setState({tiposNameData:this.tiposName()});
                    this.setState({categoriasDataData:this.categoriasData()});
                    this.setState({categoriasNameData:this.categoriasName()});
                    this.setState({tecnicosDataData:this.tecnicosData()});
                    this.setState({tecnicosNameData:this.tecnicosName()});
                    this.setState({empresasDataData:this.empresasData()});
                    this.setState({empresasNameData:this.empresasName()});
                    this.setState({inquilinosDataData:this.inquilinosData()});
                    this.setState({inquilinosNameData:this.inquilinosName()});
                    M.AutoInit();                                 
            }
            }).catch((e) => {
                console.log(e);
            });
        }
       
    }
    inquilinosName(){
        let dict = []
        this.state.inquilinos.map(inquilino=>{
            dict.push(inquilino._id.name)
        })
        return dict
    }
    inquilinosData(){
        let dict = []
        this.state.inquilinos.map(inquilino=>{
            dict.push(inquilino.total)
        })
        console.log(dict)
        return dict
    }
    tecnicosName(){
        let dict = []
        this.state.tecnicos.map(tecnico=>{
            dict.push(tecnico._id.name)
        })
        return dict
    }
    tecnicosData(){
        let dict = []
        this.state.tecnicos.map(tecnico=>{
            dict.push(tecnico.total)
        })
        console.log(dict)
        return dict
    }
    empresasName(){
        let dict = []
        this.state.empresas.map(empresa=>{
            dict.push(empresa._id.name)
        })
        return dict
    }
    empresasData(){
        let dict = []
        this.state.empresas.map(empresa=>{
            dict.push(empresa.total)
        })
        console.log(dict)
        return dict
    }
    categoriasName(){
        let dict = []
        this.state.categorias.map(categoria=>{
            dict.push(categoria._id.name)
        })
        return dict
    }
    categoriasData(){
        let dict = []
        this.state.categorias.map(categoria=>{
            dict.push(categoria.total)
        })
        console.log(dict)
        return dict
    }
    tiposName(){
        let dict = []
        this.state.tipos.map(tipo=>{
            dict.push(tipo._id.name)
        })
        return dict
    }
    tiposData(){
        let dict = []
        this.state.tipos.map(tipo=>{
            dict.push(tipo.total)
        })
        console.log(dict)
        return dict
    }
    render(){
        return(
            
            <div>
            <AuthChecker history={this.props.history} isPublic={false} accessRequire={'isAdmin'}/>
            <Navbar />  
                <div className='container'>
                    <h3 className="blue-text">Conteos de tickets</h3>
                    <form onSubmit={this.onSubmit}>
                        <div class="row">
                            <div class="input-field col s5">
                                <input 
                                value={this.state.inicio}
                                onChange={this.onInicioChange}
                                placeholder="Busqueda" id="inicio" name="inicio" type="date" />
                                <label htmlFor="first_name">Inicio</label>
                            </div>
                            <div className="input-field col s5">
                                <input 
                                value={this.state.fin}
                                onChange={this.onFinChange}
                                placeholder="Busqueda" id="fin" name="fin" type="date" />
                                <label htmlFor="first_name">Fin</label>
                            </div>
                            <div className="col s2">
                                <button 
                                    className="btn btn-primary"
                                    type="submit"
                                >Buscar</button>
                            </div>
                        </div>
                    </form>
                    
                    <div classNmae="row">
                            <p>Tickets cerrados por cada tecnico dentro del periodo especificado</p>
                            <Line
                                data={{
                                    labels:this.state.tecnicosNameData,
                                    datasets: [{
                                        label: "Tickets cerrados por tecnico",
                                        backgroundColor: 'rgba(255, 99, 132, 1)',
                                        borderColor: 'rgba(255, 99, 132, 1)',
                                        data:this.state.tecnicosDataData,
                                        fill: false,
                                        }]
                                    }}
                                width={50}
                                height={25}
                                options={{
                                    maintainAspectRatio: true,
                                    scales: {
                                        yAxes: [{
                                          ticks: {
                                            beginAtZero: true,
                                            min: 0
                                          }    
                                        }]
                                      }
                                }}
                            />
                    </div>
                    <div classNmae="row">
                            <p>Tickets generados(Abiertos y Cerrados) de cada Empresa dentro del periodo especificado</p>
                            <Line
                                data={{
                                    labels:this.state.empresasNameData,
                                    datasets: [{
                                        label: "Tickets por empresa",
                                        backgroundColor: 'rgba(75, 192, 192, 1)',
                                        borderColor: 'rgba(75, 192, 192, 1)',
                                        data:this.state.empresasDataData,
                                        fill: false,
                                        }]
                                    }}
                                width={50}
                                height={25}
                                options={{
                                    maintainAspectRatio: true,
                                    scales: {
                                        yAxes: [{
                                          ticks: {
                                            beginAtZero: true,
                                            min: 0
                                          }    
                                        }]
                                      }
                                }}
                            />
                    </div>
                    <div classNmae="row">
                            <p>Tickets generados(Abiertos y Cerrados) por cada inquilino dentro del periodo especificado</p>
                            <Bar
                                data={{
                                    labels:this.state.inquilinosNameData,
                                    datasets: [{
                                        label: "Tickets por inquilino",
                                        backgroundColor: 'rgba(54, 162, 235, 0.2)',
                                        borderColor: 'rgba(54, 162, 235, 1)',
                                        data:this.state.inquilinosDataData,
                                        }]
                                    }}
                                width={50}
                                height={25}
                                options={{
                                    maintainAspectRatio: true,
                                    scales: {
                                        yAxes: [{
                                          ticks: {
                                            beginAtZero: true,
                                            min: 0
                                          }    
                                        }]
                                      }
                                }}
                            />
                    </div>
                    <div classNmae="row">
                            <p>Tickets generados(Abiertos y Cerrados) de cada Categoria dentro del periodo especificado</p>
                            <Bar
                                data={{
                                    labels:this.state.categoriasNameData,
                                    datasets: [{
                                        label: "Tickets por categoria",
                                        backgroundColor: 'rgba(153, 102, 255, 0.2)',
                                        borderColor: 'rgba(153, 102, 255, 1)',
                                        data:this.state.categoriasDataData,
                                        fill: false,
                                        }]
                                    }}
                                width={50}
                                height={25}
                                options={{
                                    maintainAspectRatio: true,
                                    scales: {
                                        yAxes: [{
                                          ticks: {
                                            beginAtZero: true,
                                            min: 0
                                          }    
                                        }]
                                      }
                                }}
                            />
                    </div>
                    
                    <div classNmae="row">
                            <p>Tickets generados(Abiertos y Cerrados) de cada tipo dentro del periodo especificado</p>
                            <Doughnut
                                data={{
                                    labels:this.state.tiposNameData,
                                    datasets: [{
                                        label: "Tickets por tipo",
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.2)',
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(255, 206, 86, 0.2)',
                                            'rgba(75, 192, 192, 0.2)',
                                            'rgba(153, 102, 255, 0.2)',
                                            'rgba(255, 159, 64, 0.2)'
                                        ],
                                        borderColor: [
                                            'rgba(255,99,132,1)',
                                            'rgba(54, 162, 235, 1)',
                                            'rgba(255, 206, 86, 1)',
                                            'rgba(75, 192, 192, 1)',
                                            'rgba(153, 102, 255, 1)',
                                            'rgba(255, 159, 64, 1)'
                                        ],
                                        data:this.state.tiposDataData,
                                        }]
                                    }}
                                width={50}
                                height={25}
                                options={{
                                    maintainAspectRatio: true,
                                    scales: {
                                        xAxes: [{
                                            display: false
                                         }],
                                         yAxes: [{
                                            display: false
                                        }]
                                      }
                                }}
                            />
                    </div>
                    
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    form: state.form
});
const mapDispatchToProps = (dispatch, props) => ({
    startCreateUser: (data) => dispatch(startCreateUser(data)),
    createEmpresaItems: () => createEmpresaItems(),
    changeStatus: (status) => dispatch(changeStatus(status))
});

export default connect(mapStateToProps, mapDispatchToProps)(TecnicosPage);
