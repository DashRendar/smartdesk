import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import { startCreateUser, changeStatus, createEmpresaItems } from '../actions/forms';
import Navbar from './Navbar';
import AuthChecker from '../routers/AuthChecker';
import M from 'materialize-css';
import axios from 'axios';
import { getStorage } from '../helpers/cookie';

export class CreatePage extends React.Component{
    constructor(props){       
        super(props);
    }
    state = {
        password: '',
        password2: '',
        user: {},
        error: ''
    };

    dirtyForm = () => {
        //change the status of the form reducer.
        if(this.props.form.editStatus === 'clean')
            this.props.changeStatus('dirty');
    };
    
    onSubmit = (e) => {
        e.preventDefault();
        const reqInstance = axios.create({
            headers: {
                'x-auth': getStorage('usrJwt')
            }
        });
        return reqInstance.post('/admin/passChange',{"pass":this.state.password}).then((res) => {
            this.setState({error: res.data});
        }).catch((e) => {
            console.log(e);
        });
    }
    onPasswordChange = (e) => {
        this.dirtyForm();
        let password = e.target.value;
        this.setState({password});
        setTimeout(() => {this.checkForm('password')}, 0);
    };
    onPassword2Change = (e) => {
        this.dirtyForm();
        let password2 = e.target.value;
        this.setState({password2});
        setTimeout(() => {this.checkForm('password2')}, 0);
    };
    componentDidMount(){
        
        const reqInstance = axios.create({
            headers: {
                'x-auth': getStorage('usrJwt')
            }
        });
        return reqInstance.get('/admin/data').then((res) => {
                this.setState({user: res.data});
        }).catch((e) => {
            console.log('error');
        });
    }
    checkForm = (field) => {
       
        if(field === 'password' || field === 'all' || field === 'password2'){
            if(validator.isEmpty(this.state.password)){
                this.setState({error: "El password no puede estar vacio"});
                return;
            }
            if(this.state.password.length < 6){
                this.setState({error: 'El password debe tener mas de 6 caracteres.'});
                return;
            }
            if(validator.isEmpty(this.state.password2)){
                this.setState({error: 'Los password no encajan'});
                return;
            }
            if(this.state.password2!=this.state.password){
                this.setState({error: 'Los password no encajan'});
                return;
            }
        }
        this.setState({error: ''});
    };
    
    componentDidUpdate(){
        M.AutoInit();         
    }
    render(){
        return(
            <div>
            <Navbar />  
        <div className='container'>
            <AuthChecker history={this.props.history} isPublic={false}/>
            <div className="jumbotron">
                <h3 className="blue-text">Perfil</h3>
                <h5 className="blue-text">Nombre: <span>{this.state.user.name}</span></h5>
                <h5 className="blue-text">Email: <span>{this.state.user.email}</span></h5>
                {   this.state.user.inquilino ? 
                            <div role="alert">
                             <h6 className="blue-text">Local: <span>{this.state.user.inquilino.local}</span></h6>
                             <h6 className="blue-text">Empresa: <span>{this.state.user.inquilino.empresa.name}</span></h6>
                            </div>
                            : ''}
                {   this.state.user.tecnico ? 
                            <div role="alert">
                             <h6 className="blue-text">Cargo: <span>{this.state.user.tecnico.cargo}</span></h6>
                             <h6 className="blue-text">Categoria: <span>{this.state.user.tecnico.categoria.name}</span></h6>
                            </div>
                            : ''}
                <form>
                    
                    <div className="form-group">
                        <label htmlFor="inputPassword">Password</label>
                        <input 
 	            	    type="password" 
	            	    className="form-control form-control-lg"
	            	    placeholder="Password"
                            onChange={this.onPasswordChange}
	                ></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputPassword2">Confirmar password</label>
                        <input 
                        type="password" 
                        className="form-control form-control-lg"
                        placeholder="Confirmacion Password"
                            onChange={this.onPassword2Change}
                    ></input>
                    </div>
                    <button 
                        className="btn btn-primary"
                        onClick={this.onSubmit}
                        disabled={this.state.error.length !== 0 || this.props.form.editStatus === 'clean'}
                    >Guardar</button>
                        <div style={{height: '54px'}}>
                            <hr/>
                            {this.state.error !== '' ? 
                            <div role="alert">
                              <p className="red-text">{this.state.error}</p>
                            </div>
                            : ''}
                        </div>
                </form>
            </div>
        </div>
        </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    form: state.form
});
const mapDispatchToProps = (dispatch, props) => ({
    changeStatus: (status) => dispatch(changeStatus(status))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreatePage);
