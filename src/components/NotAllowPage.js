import React from 'react';
import { Link } from 'react-router-dom';

const NotAllowPage = () => (
  <div className='container'>
    <h3>
        No tienes acceso a esta area - <Link to="/dashboard">Ir al dashboard</Link>
    </h3>
  </div>
);

export default NotAllowPage;
