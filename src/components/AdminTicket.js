import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import Navbar from './Navbar';
import AuthChecker from '../routers/AuthChecker';
import M from 'materialize-css';
import axios from 'axios';
import { getStorage } from '../helpers/cookie';

export class AdminTickets extends React.Component{
    constructor(props){       
        super(props);
    }
    state = {
        tickets: [],
        user: {
            privilige: [{
                isAdmin: false
            }],
        },
        tipos: [],
        inquilinos: [],
        descripcion: "",
        tipo: "",
        inquilino:""
    };
    onSelectChange = (e) => {
        let dato = e.target.value;
        this.setState({tipo:dato});
    };
    onDescripcionChange = (e) => {
        let dato = e.target.value;
        this.setState({descripcion:dato});
    };
    onInquilinoChange = (e) => {
        let dato = e.target.value;
        this.setState({inquilino:dato});
    };
    getInquilinos = async () => {
        try {
          return await axios.get('/admin/inquilinos/active')
        } catch (error) {
          console.error(error)
        }
      }
     
    async selectInquilinos() {
        const query = await this.getInquilinos()
        let inquilinos = query.data;
        return inquilinos;         
      }

    
    async componentDidMount(){
        this.update()        
        const reqInstance = axios.create({
            headers: {
                'x-auth': getStorage('usrJwt')
            }
        });
        return reqInstance.get('/admin/data').then((res) => {
                this.setState({user: res.data});
                if(!res.data.privilige[0].isAdmin){
                    this.setState({inquilino: res.data._id});                
                }
                M.AutoInit(); 
            }).catch((e) => {
            console.log('error');
        });
    }
    getTipos = async () => {
        try {
          return await axios.get('/categoria/tipos/all')
        } catch (error) {
          console.error(error)
        }
      }

    async selectTipos() {
        const query = await this.getTipos()
        let tipos = query.data;
        return tipos;         
      }
    
    getTickets = async () => {
        const reqInstance = axios.create({
            headers: {
                'x-auth': getStorage('usrJwt')
            }
        });
        return reqInstance.get('/tickets/administrativos').then((res) => {
                return res;
        }).catch((e) => {
            console.log('error');
        });
      }

    async selectTickets() {
        const query = await this.getTickets()
        let tickets = query.data;
        return tickets;         
      }


    async update(){
        let tipado = await this.selectTipos();  
        this.setState({tipos:tipado});
        let inquis = await this.selectInquilinos();  
        this.setState({inquilinos:inquis});
        let tiquetes = await this.selectTickets();  
        this.setState({tickets:tiquetes});
        M.AutoInit()
    }

     changeState = async(e) =>{
        try {
            let result = await axios.get('/tickets/state?id='+e.target.id)
            await this.update()
          } catch (error) {
            console.error(error)
          }
    }

    render(){
                return(
            <div>
            <AuthChecker history={this.props.history} isPublic={false} accessRequire={'isAdmin'}/>
            <Navbar />  
                <div style={{margin: '2rem'}}>
                <h3 className="blue-text">Tickets Cerrados Administrativamente</h3>
                <table className="centered">
                    <thead>
                    <tr>
                        <th>Descripcion</th>
                        <th>Tipo</th>
                            <th>Inquilino</th>
                            <th>Tecnico</th>
                        <th>Fecha de apertura</th>
                        <th>Fecha de asignacion</th>
                        <th>Fecha de cierre</th>
                        <th>Cerrar Ticket</th>
                        
                    </tr>
                    </thead>
                        
                    <tbody>
                        {
                            this.state.tickets.map (ticket => {
                                return(
                                    <tr key={ticket._id}>
                                        <td>{ticket.description}</td>
                                        <td>{ticket.tipo.name}</td>
                                            <td>{ticket.inquilino.name}</td>
                                            <td>{ticket.tecnico? ticket.tecnico.name:"No se asigno tecnico"}</td>
                                        <td>{new Date(ticket.apertura).toLocaleDateString("es-GT",{ year: 'numeric', month: 'numeric', day: 'numeric' })}</td>
                                        <td>{ticket.asignado? new Date(ticket.asignado).toLocaleDateString("es-GT",{ year: 'numeric', month: 'numeric', day: 'numeric' }):"No se asigno tecnico"}</td>
                                        <td>{ticket.cierre? new Date(ticket.cierre).toLocaleDateString("es-GT",{ year: 'numeric', month: 'numeric', day: 'numeric' }):"Aun no se ah cerrado"}</td>
                                        <td><a onClick={this.changeState}  className="btn-floating red"><i id={ticket._id}className="material-icons">close</i></a></td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                </div>
            </div>
        );
    }  
}

const mapStateToProps = (state, props) => ({
    form: state.form
});
const mapDispatchToProps = (dispatch, props) => ({
    startCreateUser: (data) => dispatch(startCreateUser(data)),
    createEmpresaItems: () => createEmpresaItems(),
    changeStatus: (status) => dispatch(changeStatus(status))
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminTickets);
