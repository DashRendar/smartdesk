import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import Navbar from './Navbar';
import AuthChecker from '../routers/AuthChecker';
import M from 'materialize-css';
import axios from 'axios';
import { getStorage } from '../helpers/cookie';
import {Bar,Doughnut,Line} from 'react-chartjs-2';
export class TecnicosPage extends React.Component{
    constructor(props){       
        super(props);
    }
    state = {
        tecnico: "",
        tipo:"",
        tipos: [],
        url:"http://localhost:5003/",
        tecnicos: [],        
        flag: false
    };
    
    onSelectChange = (e) => {
        let dato = e.target.value;
        this.setState({tipo:dato});
        this.setState({flag:false});        
        this.update(dato);
    };
    getTecnicos = async () => {
        try {
          return await axios.get('/admin/tecnicos/active')
        } catch (error) {
          console.error(error)
        }
      }
     
    async selectTecnicos() {
        const query = await this.getTecnicos()
        let tecnicos = query.data;
        return tecnicos;         
      }
      getTipos = async () => {
        try {
          return await axios.get('/categoria/tipos/all')
        } catch (error) {
          console.error(error)
        }
      }

    async selectTipos() {
        const query = await this.getTipos()
        let tipos = query.data;
        return tipos;         
      }
      async componentDidMount(){
        let tipado = await this.selectTipos();  
        this.setState({tipos:tipado});
        let tecnics = await this.selectTecnicos();  
        this.setState({tecnicos:tecnics});
        M.AutoInit()
    }
    load(uid,dato){
        const reqInstance = axios.create({
            headers: {
                'x-auth': getStorage('usrJwt')
            }
        });
        return reqInstance.post(this.state.url+"tecnico", {"tecnico":uid,"tipo":dato}).then((res) => {
                let ms = res.data.prediccion
                let mili = ms%1000; ms -= mili; ms /= 1000;
                let segs = ms%60; ms -= segs; ms /= 60;
                let mins = ms%60; ms -= mins; ms /= 60;
                let horas = ms;
                let predi = horas+" horas con "+mins+" minutos"
                if(res.data.prediccion<0){
                    predi = "Informacion insuficiente"
                }
                return(predi);
            }).catch((e) => {
            console.log(e);
        });
    }
    update = (dato)=>{
        let itemsProcessed = 0
        this.state.tecnicos.forEach((element, index, array)=>{
            this.load(element.uid,dato).then((data)=>{
                element.prediccion = data  
                itemsProcessed++;
                if(itemsProcessed === array.length) {
                    this.setState({flag:true});
                    console.log(this.state.tecnicos)
                }            
            })    
        });
    }
    render(){
        return(
            
            <div>
            <AuthChecker history={this.props.history} isPublic={false} accessRequire={'isAdmin'}/>
            <Navbar />  
                <div className='container'>
                    <h3 className="blue-text">Pronostico de tiempos</h3>
                    <form onSubmit={this.onSubmit}>
                        <div class="row">
                            <div className="input-field col s12">
                                <select value={this.state.tipo} name="tipos" onChange={this.onSelectChange}>
                                    <option value="" defaultValue>Tipos</option>
                                {   
                                        this.state.tipos.map (tipo => {
                                            return(
                                                <option value={tipo.uid}>{tipo.category.name}-{tipo.name}</option>
                                            )
                                        })
                                }
                                </select>
                                <label>Tipos</label>
                            </div>
                        </div>
                    </form>
                    <table className="centered">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Cargo</th>
                        <th>Categoria</th>
                        <th>Tiempo estimado de cierre</th>
                    </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.tecnicos.map (tecnico => {
                                return(
                                    <tr key={tecnico._id}>
                                        <td>{tecnico.name}</td>
                                        <td>{tecnico.tecnico.cargo}</td>
                                        <td>{tecnico.tecnico.categoria.name}</td>
                                        {
                                            this.state.flag ?
                                                <td>{tecnico.prediccion}</td>:""
                                        }
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    form: state.form
});
const mapDispatchToProps = (dispatch, props) => ({
    startCreateUser: (data) => dispatch(startCreateUser(data)),
    createEmpresaItems: () => createEmpresaItems(),
    changeStatus: (status) => dispatch(changeStatus(status))
});

export default connect(mapStateToProps, mapDispatchToProps)(TecnicosPage);
