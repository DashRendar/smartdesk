import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import { startCreateUser, changeStatus, createEmpresaItems } from '../actions/forms';
import Navbar from './Navbar';
import AuthChecker from '../routers/AuthChecker';
import M from 'materialize-css';
import axios from 'axios';

export class CreatePage extends React.Component{
    constructor(props){       
        super(props);
    }
    state = {
        items: [],
        categoria: [],
        email: '',
        password: 'contraseña',
        name: '',
        privilige: {
            isActivate: true,
            isStaff: false,
            isInquilino: false,
            isAdmin: false
        },
        tecnico:{
            categoria: '',
            cargo: ''
        },
        inquilino:{
            local: '',
            empresa: ''
        },
        error: '',
    };

    getEmpresa = async () => {
        try {
          return await axios.get('/empresa/allx')
        } catch (error) {
          console.error(error)
        }
      }

    async selectEmpresa() {
        const query = await this.getEmpresa()
        let empresas = query.data;
        let items = []        
        for( let item of empresas){
            await items.push(<option key={item._id} value={item._id}>{item.name}</option>);  
        }
        return items;         
      }

    getCategoria = async () => {
        try {
          return await axios.get('/categoria/allx')
        } catch (error) {
          console.error(error)
        }
      }

    async selectCategoria() {
        const query = await this.getCategoria()
        let categorias = query.data;
        let items = []        
        for( let item of categorias){
            await items.push(<option key={item._id} value={item._id}>{item.name}</option>);  
        }
        return items;         
      }

    dirtyForm = () => {
        //change the status of the form reducer.
        if(this.props.form.editStatus === 'clean')
            this.props.changeStatus('dirty');
    };
    
    onSubmit = (e) => {
        e.preventDefault();
       
        setTimeout(() => {this.checkForm('all')}, 0);
        this.props.startCreateUser(this.state);
       
    }
    onEmailChange = (e) => {
        this.dirtyForm();
        let email = e.target.value;
        this.setState({email});
        setTimeout(() => {this.checkForm('email')}, 0);
    };
    onNameChange = (e) => {
        this.dirtyForm();
        let name = e.target.value;
        this.setState({name});
        setTimeout(() => {this.checkForm('name')}, 0);
    };
    onCargoChange = (e) => {
        this.dirtyForm();
        let cargo = e.target.value;
        let target = e.target.name;
        let tecnico = {
            ...this.state.tecnico,
        }
        tecnico[target] = cargo
        this.setState({tecnico});
        setTimeout(() => {this.checkForm('cargo')}, 0);
    };
    onSelectChange = (e) => {
        this.dirtyForm();
        let cargo = e.target.value;
        let target = e.target.name;
        let tecnico = {
            ...this.state.tecnico,
        }
        tecnico[target] = cargo
        this.setState({tecnico});
        setTimeout(() => {this.checkForm('categoria')}, 0);
    };
    onLocalChange = (e) => {
        this.dirtyForm();
        let cargo = e.target.value;
        let target = e.target.name;
        let inquilino = {
            ...this.state.inquilino,
        }
        inquilino[target] = cargo
        this.setState({inquilino});
        setTimeout(() => {this.checkForm('local')}, 0);
    };
    onEmpresaChange = (e) => {
        this.dirtyForm();
        let empresa = e.target.value;
        let target = e.target.name;
        let inquilino = {
            ...this.state.inquilino,
        }
        inquilino[target] = empresa
        this.setState({inquilino});
        setTimeout(() => {this.checkForm('empresa')}, 0);
    };
    onClickCheck = async(e) => {
        let target = e.target.name;
        let val = !e.target.value; 
        let privilige = {
            ...this.state.privilige,
        };
        privilige[target] = val;
        if(privilige.isInquilino && target=="isInquilino"){
            privilige.isAdmin = false;
            privilige.isStaff = false;
        }
        else if(privilige.isAdmin || privilige.isStaff){
            privilige.isInquilino = false;
        }
        this.setState({privilige});
        setTimeout(() => {this.checkForm('all')}, 0);
        
    }
    onPasswordChange = (e) => {
        this.dirtyForm();
        let password = e.target.value;
        this.setState({password});
        setTimeout(() => {this.checkForm('password')}, 0);
    };
    checkForm = (field) => {
        if(field === 'email' || field === 'all'){
            if(!validator.isEmail(this.state.email)){
                this.setState({error: 'Formato de correo invalido'});
                return;
            }
        }
        if(field === 'name' || field === 'all'){
            if(validator.isEmpty(this.state.name)){
                this.setState({error: "El nombre no puede estar vacio"});
                return;
            }
            if(this.state.name.length < 6){
                this.setState({error: 'El nombre debe contener almenos 6 letras.'});
                return;
            }
        }

        if(field === 'password' || field === 'all'){
            if(validator.isEmpty(this.state.password)){
                this.setState({error: "El password no puede estar vacio"});
                return;
            }
            if(this.state.password.length < 6){
                this.setState({error: 'El password debe tener mas de 6 caracteres.'});
                return;
            }
        }
        if(field === 'cargo' || field === 'all'){
            if(this.state.privilige.isStaff){
                if(validator.isEmpty(this.state.tecnico.cargo)){
                    this.setState({error: 'El cargo del tecnico no puede estar vacio.'});
                    return;
                }
            }
        }
        if(field === 'local' || field === 'all'){
            if(this.state.privilige.isInquilino){
                if(validator.isEmpty(this.state.inquilino.local)){
                    this.setState({error: 'El local del inquilino no puede estar vacio.'});
                    return;
                }
            }
        }
        if(field === 'categoria' || field === 'all'){
            if(this.state.privilige.isStaff){
                if(this.state.tecnico.categoria==""){
                    this.setState({error: 'Debe asignar la empresa del inquilino'});
                    return;
                }
            }
        }
        if(field === 'empresa' || field === 'all'){
            if(this.state.privilige.isInquilino){
                if(this.state.inquilino.empresa==""){
                    this.setState({error: 'Debe asignar la empresa del inquilino'});
                    return;
                }
            }
        }
        if(field==='all'){
            if(this.state.privilige.isStaff == false &&this.state.privilige.isInquilino == false &&this.state.privilige.isAdmin == false){
                this.setState({error: 'El usuario debe tener al menos un tipo de rol'});
                return;
            }
        }
        this.setState({error: ''});
    };
    onInputCheck = (e) => {
        this.dirtyForm();
        let target = e.target.name;
        let value = e.target.value;
        let privilige = {
            ...this.state.privilige,
        };
        privilige[target] = e.target.checked;
        if(privilige.isInquilino && target=="isInquilino"){
            privilige.isAdmin = false;
            privilige.isStaff = false;
        }
        else if(privilige.isAdmin || privilige.isStaff){
            privilige.isInquilino = false;
        }
        this.setState({privilige});
    };
    async componentDidMount(){
        this.state.items = await this.selectEmpresa();           
        this.state.categoria = await this.selectCategoria();           
    }
    componentDidUpdate(){
        M.AutoInit();         
        if(this.props.form.editStatus === 'success'){
            setTimeout(() => {
                this.props.history.push('/'); //redirect to the main page after submit the form successfully.
            }, 1000);
        }else if(this.props.form.editStatus === 'error'){
            setTimeout(() => {this.checkForm('all')}, 0);
            this.setState({error: 'El local del inquilino no puede estar vacio.'});
            this.props.changeStatus('dirty');
        }else if(this.props.form.editStatus === 'Correo ya existe Invalido'){
            this.setState({error: 'El correo ya existe.'});
            this.props.changeStatus('dirty');
        }
    }
    render(){
        return(
            <div>
            <Navbar />  
        <div className='container'>
            <AuthChecker history={this.props.history} isPublic={false} accessRequire={'isAdmin'}/>
            <div className="jumbotron">
                <h3 className="display-4">Crear Usuario</h3>
                <form>
                    <div className="form-group">
                        <label htmlFor="inputEmail">Direccion de correo</label>
                        <input 
                            type="email" 
                            className="form-control form-control-lg" 
                            aria-describedby="emailHelp" 
                            placeholder="Ingrese Email"
                            value={this.state.email}
                            onChange={this.onEmailChange}
                         ></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputName">Nombre</label>
                        <input 
                            type="text" 
	                    className="form-control form-control-lg" 
	            	    aria-describedby="nameHelp" 
	            	    placeholder="Ingrese Nombre de inquilino, tienda o tecnico"
                            value={this.state.name}
                            onChange={this.onNameChange}
	                ></input>
                    </div>
                    <div className="form-group" hidden>
                        <label htmlFor="inputPassword">Password</label>
                        <input 
 	            	    type="password" 
	            	    className="form-control form-control-lg"
	            	    placeholder="Password"
                            value="contraseña"
                            onChange={this.onPasswordChange}
	                ></input>
                    </div>
                    <div className="form-group">
                    <div className="form-check">
                        <label>
                            <input name="isInquilino" 
                                type="radio" 
                                checked={this.state.privilige.isInquilino} 
                                onChange={this.onInputCheck}
                                onClick={this.onClickCheck}/>
                            <span>Inquilino</span>
                        </label>
                    </div>
	            	<div className="form-check">
                        <label>
                            <input name="isStaff" 
                                type="radio" 
                                checked={this.state.privilige.isStaff} 
                                onChange={this.onInputCheck}
                                onClick={this.onClickCheck}/>
                            <span>Tecnico</span>
                        </label>
                    </div>
	            	<div className="form-check">
                        <label>
                            <input name="isAdmin" 
                                type="radio" 
                                checked={this.state.privilige.isAdmin} 
                                onChange={this.onInputCheck}
                                onClick={this.onClickCheck}/>
                            <span>Administrador</span>
                        </label>
                    </div>
                    </div>
                    <div hidden={!this.state.privilige.isStaff}>
                        <div className="form-group">
                            <label>Cargo</label>
                            <input 
                            type="text" 
                            name="cargo"
                            onChange={this.onCargoChange}/>
                        </div>
                        <div className="form-group">
                        <div className="input-field col s12">
                            <select name="categoria" onChange={this.onSelectChange}>
                            <option value="" disabled selected>Seleccionar una categoria</option>
                            {this.state.categoria}
                            </select>
                            <label>Categoria</label>
                        </div>
                        </div>

                    </div>
                    <div hidden={!this.state.privilige.isInquilino}>
                        <div className="form-group">
                            <label>Numero de local</label>
                            <input 
                            type="text" 
                            name="local"
                            onChange={this.onLocalChange}/>
                        </div>
                        <div className="form-group">
                        <div className="input-field col s12">
                            <select name="empresa" onChange={this.onEmpresaChange}>
                                <option value="" disabled selected>Seleccionar una empresa</option>
                                {this.state.items}
                            </select>
                            <label>Empresa</label>
                        </div>
                        </div>

                    </div>
                    <button 
                        className="btn btn-primary"
                        onClick={this.onSubmit}
                        disabled={this.state.error.length !== 0 || this.props.form.editStatus === 'clean'}
                    >Guardar</button>
                        <div style={{height: '54px'}}>
                            <hr/>
                            {this.state.error !== '' ? 
                            <div role="alert">
                              <p className="red-text">{this.state.error}</p>
                            </div>
                            : ''}
                        </div>
                </form>
            </div>
        </div>
        </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    form: state.form
});
const mapDispatchToProps = (dispatch, props) => ({
    startCreateUser: (data) => dispatch(startCreateUser(data)),
    createEmpresaItems: () => createEmpresaItems(),
    changeStatus: (status) => dispatch(changeStatus(status))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreatePage);
