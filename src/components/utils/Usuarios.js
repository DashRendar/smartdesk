import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (state, props) => ({
    auth: state.auth
});

export class Usuarios extends React.Component{
    constructor(props){
        super(props);
    }

    render(){     
    try{
        if(this.props.auth.access.isAdmin === true){
            return(
                <a className="dropdown-trigger" href="#!" data-target="dropdown1">Usuarios<i className="material-icons right">arrow_drop_down</i></a>
            );
        }
    }catch(err){
    }
        return(<div></div>);
    }  
}


export default connect(mapStateToProps)(Usuarios);
