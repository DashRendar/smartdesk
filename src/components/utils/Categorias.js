import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (state, props) => ({
    auth: state.auth
});

export class Categorias extends React.Component{
    constructor(props){
        super(props);
    }

    render(){     
    try{
        if(this.props.auth.access.isAdmin === true){
            return(
                <a href="/categorias">Categorias</a>
            );
        }
    }catch(err){
    }
        return(<div></div>);
    }  
}


export default connect(mapStateToProps)(Categorias);
