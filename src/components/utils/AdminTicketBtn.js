import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (state, props) => ({
    auth: state.auth
});

export class AdminTicketBtn extends React.Component{
    constructor(props){
        super(props);
    }

    render(){     
    try{
        if(this.props.auth.access.isAdmin === true){
            return(
                <li><a href="/tiquetes/administrativos">Cerrados por Adminsitracion</a></li>
            );
        }
    }catch(err){
    }
        return(<div></div>);
    }  
}


export default connect(mapStateToProps)(AdminTicketBtn);
