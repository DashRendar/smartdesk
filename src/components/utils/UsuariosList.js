import React from 'react';
import { connect } from 'react-redux';
import CreateBtn from './CreateBtn';

const mapStateToProps = (state, props) => ({
    auth: state.auth
});

export class UsuariosList extends React.Component{
    constructor(props){
        super(props);
    }

    render(){     
    try{
        if(this.props.auth.access.isAdmin === true){
            return(
                <div>
                    <li><a href="/tecnicos">Tecnicos</a></li>
                    <li><a href="/inquilinos">Inquilinos</a></li>
                    <li><a href="/administradores">Administradores</a></li>
                    <li className="divider" tabIndex="-1"></li>
                    <li><CreateBtn /></li>
                    <li className="divider" tabIndex="-1"></li>
                </div>
            );
        }
    }catch(err){
    }
        return(<div></div>);
    }  
}


export default connect(mapStateToProps)(UsuariosList);
