import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import Navbar from './Navbar';
import AuthChecker from '../routers/AuthChecker';
import M from 'materialize-css';
import axios from 'axios';
import { getStorage } from '../helpers/cookie';

export class Categorias extends React.Component{
    constructor(props){       
        super(props);
    }
    state = {
        categorias: [],
        nombre: "",
        descripcion: "",
        find:"",
        tipo_nombre: "",
        tipo_descripcion: "",
    };
    onTipoDChange = (e) => {
        let dato = e.target.value;
        this.setState({tipo_descripcion:dato});
    };
    onTipoNChange = (e) => {
        let dato = e.target.value;
        this.setState({tipo_nombre:dato});
    };
    onAddTipo = (e) => {
        if(this.state.tipo_descripcion=="" || this.state.tipo_nombre==""){
            M.toast({html: "Formulario no puede estar vacio"})            
        }else{
            e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/categoria/addtipo', {"description":this.state.tipo_descripcion,"name":this.state.tipo_nombre,"category":e.target.id}).then((res) => {
                this.setState({tipo_descripcion:""});
                this.setState({tipo_nombre:""});
                this.update();    
                M.toast({html: res.data})
            }).catch((e) => {
                console.log('error');
            });
        } 
        
       
    }
    removeTipo = (e) => {
        const reqInstance = axios.create({
            headers: {
                'x-auth': getStorage('usrJwt')
            }
        });
        return reqInstance.post('/categoria/rmtipo', {"data":e.target.id}).then((res) => {
            this.update();    
            M.toast({html: res.data})
        }).catch((e) => {
            console.log('error');
        });
    }
    onNombreChange = (e) => {
        let dato = e.target.value;
        this.setState({nombre:dato});
    };
    onSubmit = (e) => {
        e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/categoria/find', {"dato":this.state.find}).then((res) => {
                this.setState({categorias:res.data});  
                M.AutoInit();                                 
            }).catch((e) => {
                console.log('error');
            });
       
    }
    onFindChange = (e) => {
        let dato = e.target.value;
        this.setState({find:dato});
    };
    onDescripcionChange = (e) => {
        let dato = e.target.value;
        this.setState({descripcion:dato});
    };
    onAddCategoria = (e) => {
        if(this.state.descripcion=="" || this.state.nombre==""){
            M.toast({html: "Formulario no puede estar vacio"})            
        }else{
            e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/categoria/add', {"descripcion":this.state.descripcion,"nombre":this.state.nombre}).then((res) => {
                this.setState({nombre:""});
                this.setState({descripcion:""});
                this.update();    
                M.toast({html: res.data})
            }).catch((e) => {
                console.log('error');
            });
        } 
        
       
    }
    getCategorias = async () => {
        try {
          return await axios.get('/categoria/all')
        } catch (error) {
          console.error(error)
        }
      }
      onDatoChange = (e) => {
        let dato = e.target.value;
        this.setState({dato});
    };
    async selectCategorias() {
        const query = await this.getCategorias()
        let categorias = query.data;
        return categorias;         
      }

    async componentDidMount(){
        await this.update()        
        M.AutoInit();                 
    }

    async update(){
        let data = await this.selectCategorias(); 
        await this.setState({categorias:data});        
    }

     changeState = async(e) =>{
        try {
            let result = await axios.get('/categoria/state?id='+e.target.id)
            await this.update()            
          } catch (error) {
            console.error(error)
          }
    }
    render(){
        return(
            <div>
            <AuthChecker history={this.props.history} isPublic={false} accessRequire={'isAdmin'}/>
            <Navbar />  
                <div className='container'>
                <h3 className="blue-text">Categorias registradas</h3>
                <form>
                <div className="row">
                    <div className="input-field col s5">
                        <input 
                        value={this.state.find}
                        onChange={this.onFindChange}
                        placeholder="Busqueda" id="find" name="find" type="text" className="validate"/>
                        <label htmlFor="first_name">Busqueda</label>
                    </div>
                    <div className="col s2">
                        <button 
                            className="btn btn-primary"
                            onClick={this.onSubmit}
                        >Buscar</button>
                    </div>
                </div>
                </form>
                <div className="row">
                    <div className="col s2 push-s10">
                        <a className="btn-floating yellow modal-trigger" href="#modal1"><i className="material-icons">add</i></a>

                    <div id="modal1" className="modal">
                        <div className="modal-content">
                            <h4>Agregar Categoria</h4>
                            <form>
                                <div className="row">
                                    
                                    <div className="input-field col s5">
                                        <input required="required"
                                        value={this.state.nombre}
                                        onChange={this.onNombreChange}
                                        id="nombre" name="nombre" type="text" className="validate"/>
                                        <label htmlFor="nombre">Nombre</label>
                                    </div>
                                    <div className="input-field col s5">
                                        <input required="required"
                                        value={this.state.descripcion}
                                        onChange={this.onDescripcionChange}
                                        id="descripcion" name="descripcion" type="text" className="validate"/>
                                        <label htmlFor="descripcion">Descripcion</label>
                                    </div>
                                    <div className="col s2">
                                        <button 
                                            className="btn btn-primary"
                                            onClick={this.onAddCategoria}
                                        >Agregar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
                {
                    this.state.categorias.map (categoria => {
                        return(
                        <div key={categoria._id} id={categoria._id} className="modal">
                            <div className="modal-content">
                                <h4>Tipos de ticket: {categoria.name}</h4>
                                <div className="row">
                                <form>
                                <div className="input-field col s5">
                                        <input required="required"
                                        value={this.state.tipo_nombre}
                                        onChange={this.onTipoNChange}
                                        id="nombre" placeholder=" " name="nombre" type="text" className="validate"/>
                                        <label htmlFor="nombre">Nombre</label>
                                    </div>
                                <div className="row">
                                    <div className="input-field col s5">
                                        <input required="required"
                                        value={this.state.tipo_descripcion}
                                        onChange={this.onTipoDChange}
                                        id="descripcion" placeholder=" " name="descripcion" type="text" className="validate"/>
                                        <label htmlFor="descripcion">Descripcion</label>
                                    </div>
                                    
                                    <div className="col s2">
                                        <button 
                                            id={categoria._id}
                                            className="btn btn-primary"
                                            onClick={this.onAddTipo}
                                        >Agregar</button>
                                    </div>
                                </div>
                                </form>
                                </div>
                                <table className="centered">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {categoria.tipos.map(tipo =>{
                                            return(
                                                <tr key={tipo._id}>
                                                    <td>{tipo.name}</td>
                                                    <td>{tipo.description}</td>
                                                    <td>{tipo.isDeleted ? 
                                            <a onClick={this.removeTipo}  className="btn-floating red"><i id={tipo._id}className="material-icons">close</i></a>:<a onClick={this.removeTipo}  className="btn-floating blue"><i id={tipo._id}className="material-icons">check_circle</i></a>}</td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        )
                    })
                }
                <table className="centered">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Tipos</th>
                        <th>Cambiar estado</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                        {
                            this.state.categorias.map (categoria => {
                                return(
                                    <tr key={categoria._id}>
                                        <td>{categoria.name}</td>
                                        <td>{categoria.description}</td>
                                        <td><a className="waves-effect btn-floating red modal-trigger" href={"#"+categoria._id}><i className="material-icons">apps</i></a></td>
                                        <td>{categoria.isDeleted ? 
                                            <a onClick={this.changeState}  className="btn-floating red"><i id={categoria._id}className="material-icons">close</i></a>:<a onClick={this.changeState}  className="btn-floating blue"><i id={categoria._id}className="material-icons">check_circle</i></a>}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    form: state.form
});
const mapDispatchToProps = (dispatch, props) => ({
    changeStatus: (status) => dispatch(changeStatus(status))
});

export default connect(mapStateToProps, mapDispatchToProps)(Categorias);
