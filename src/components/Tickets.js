import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import Navbar from './Navbar';
import AuthChecker from '../routers/AuthChecker';
import M from 'materialize-css';
import axios from 'axios';
import { getStorage } from '../helpers/cookie';

export class TicketsPage extends React.Component{
    constructor(props){       
        super(props);
    }
    state = {
        tickets: [],
        user: {
            privilige: [{
                isAdmin: false
            }],
        },
        tipos: [],
        inquilinos: [],
        descripcion: "",
        tecnicos: [],
        tecnico: "",
        prioridad: "",
        tipo: "",
        inquilino:""
    };
    onSelectChange = (e) => {
        let dato = e.target.value;
        this.setState({tipo:dato});
    };
    onTecnicoChange = (e) => {
        let dato = e.target.value;
        this.setState({tecnico:dato});
    };
    onPrioridadChange = (e) => {
        let dato = e.target.value;
        this.setState({prioridad:dato});
    };
    onDescripcionChange = (e) => {
        let dato = e.target.value;
        this.setState({descripcion:dato});
    };
    onInquilinoChange = (e) => {
        let dato = e.target.value;
        this.setState({inquilino:dato});
    };
    getInquilinos = async () => {
        try {
          return await axios.get('/admin/inquilinos/active')
        } catch (error) {
          console.error(error)
        }
      }
     
    async selectInquilinos() {
        const query = await this.getInquilinos()
        let inquilinos = query.data;
        return inquilinos;         
      }
      getTecnicos = async () => {
        try {
          return await axios.get('/admin/tecnicos/active')
        } catch (error) {
          console.error(error)
        }
      }
     
    async selectTecnicos() {
        const query = await this.getTecnicos()
        let tecnicos = query.data;
        return tecnicos;         
      }
    onAssign= (e) => {
        e.preventDefault();        
        if(this.state.prioridad=="" || this.state.tecnico==""){
            M.toast({html: "Por favor llene todos los campos del formulario"})    
        }else{
            e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/tickets/assign', {"prioridad":this.state.prioridad,"tecnico":this.state.tecnico,"id":e.target.id}).then((res) => {
                this.setState({prioridad:""});
                this.setState({tecnico:""});
                this.update();    
                M.toast({html: res.data})
            }).catch((e) => {
                console.log('error');
            });
        } 
        
       
    }
    onAddTicket = (e) => {
        e.preventDefault();        
        if(this.state.inquilino=="" || this.state.tipo=="" || this.state.descripcion==""){
            M.toast({html: "Por favor llene todos los campos del formulario"})    
        }else{
            e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/tickets/add', {"descripcion":this.state.descripcion,"inquilino":this.state.inquilino,"tipo":this.state.tipo}).then((res) => {
                this.setState({inquilino:""});
                this.setState({tipo:""});
                this.setState({descripcion:""});
                this.update();    
                console.log(this.state);
                M.toast({html: res.data})
            }).catch((e) => {
                console.log('error');
            });
        } 
        
       
    }
    async componentDidMount(){
        this.update()        
        const reqInstance = axios.create({
            headers: {
                'x-auth': getStorage('usrJwt')
            }
        });
        return reqInstance.get('/admin/data').then((res) => {
                this.setState({user: res.data});
                if(!res.data.privilige[0].isAdmin){
                    this.setState({inquilino: res.data._id});                
                }
                M.AutoInit(); 
            }).catch((e) => {
            console.log('error');
        });
    }
    getTipos = async () => {
        try {
          return await axios.get('/categoria/tipos/all')
        } catch (error) {
          console.error(error)
        }
      }

    async selectTipos() {
        const query = await this.getTipos()
        let tipos = query.data;
        return tipos;         
      }
    
    getTickets = async () => {
        const reqInstance = axios.create({
            headers: {
                'x-auth': getStorage('usrJwt')
            }
        });
        return reqInstance.get('/tickets/abiertos').then((res) => {
                return res;
        }).catch((e) => {
            console.log('error');
        });
      }

    async selectTickets() {
        const query = await this.getTickets()
        let tickets = query.data;
        return tickets;         
      }


    async update(){
        let tipado = await this.selectTipos();  
        this.setState({tipos:tipado});
        let tecnics = await this.selectTecnicos();  
        this.setState({tecnicos:tecnics});
        let inquis = await this.selectInquilinos();  
        this.setState({inquilinos:inquis});
        let tiquetes = await this.selectTickets();  
        this.setState({tickets:tiquetes});
        M.AutoInit()
    }

     changeState = async(e) =>{
        try {
            let result = await axios.get('/tickets/state?id='+e.target.id)
            await this.update()
          } catch (error) {
            console.error(error)
          }
    }

    render(){
        try{
                return(
            <div>
            <AuthChecker history={this.props.history} isPublic={false}/>
            <Navbar />  
            {
                    this.state.tickets.map (ticket => {
                        return(
                        <div key={ticket._id} id={ticket._id} className="modal">
                            <div className="modal-content">
                                <p>Contactos {ticket.description}</p>
                                <form>
                                <div className="row">
                                <div className="input-field col s8">
                                    <select value={this.state.tecnico} name="tecnicos" onChange={this.onTecnicoChange}>
                                        <option value="" defaultValue>Seleccione un tecnico</option>
                                        {   
                                                this.state.tecnicos.map (tecnico => {
                                                    return(
                                                        <option value={tecnico._id}>{tecnico.name} - {tecnico.tecnico.cargo}</option>
                                                    )
                                                })
                                        }
                                    </select>
                                    <label>Inquilinos</label>
                                </div>
                                    <div className="input-field col s4">
                                    <select value={this.state.prioridad} name="prioridad" onChange={this.onPrioridadChange}>
                                        <option value="" defaultValue>Seleccione prioridad</option>
                                        <option value="4" >Relevante</option>
                                        <option value="3" >Requiere Atencion</option>
                                        <option value="2" >Urgente</option>
                                        <option value="1" >Emergencia</option>
                                    </select>
                                    <label>Estado</label>
                                    </div>
                                    <div className="col s2">
                                        <button 
                                            id={ticket._id}
                                            className="btn btn-primary"
                                            onClick={this.onAssign}
                                        >Agregar</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        )
                    })
                }
                <div style={{margin: '2rem'}}>
                <h3 className="blue-text">Tickets Abiertos</h3>
                <div className="row">
                <div className="col s2 push-s10">
                        <a className="btn-floating yellow modal-trigger" href="#modal1"><i className="material-icons">add</i></a>
                </div>
                </div>
                <table className="centered">
                    <thead>
                    <tr>
                        <th>Descripcion</th>
                        <th>Tipo</th>
                        {
                        this.state.user.privilige[0].isAdmin ? 
                            <th>Inquilino</th>:""
                        }
                        <th>Tecnico</th>
                        <th>Fecha de apertura</th>
                        <th>Fecha de asignacion</th>
                        <th>Fecha de cierre</th>
                        {
                        this.state.user.privilige[0].isAdmin ? 
                            <th>Asignar Ticket</th>:""
                        }
                        <th>Cerrar Ticket</th>
                    </tr>
                    </thead>
                        
                    <tbody>
                        {
                            this.state.tickets.map (ticket => {
                                return(
                                    <tr key={ticket._id}>
                                        <td>{ticket.description}</td>
                                        <td>{ticket.tipo.name}</td>
                                        {
                                        this.state.user.privilige[0].isAdmin ? 
                                            <td>{ticket.inquilino.name}</td>:""
                                        }
                                        <td>{ticket.tecnico? ticket.tecnico.name:"Aun no se ah asignado un tecnico"}</td>
                                        <td>{new Date(ticket.apertura).toLocaleDateString("es-GT",{ year: 'numeric', month: 'numeric', day: 'numeric' })}</td>
                                        <td>{ticket.asignado? new Date(ticket.asignado).toLocaleDateString("es-GT",{ year: 'numeric', month: 'numeric', day: 'numeric' }):"Aun no se ah asignado el ticket"}</td>
                                        <td>{ticket.cierre? new Date(ticket.cierre).toLocaleDateString("es-GT",{ year: 'numeric', month: 'numeric', day: 'numeric' }):"Aun no se ah cerrado"}</td>
                                        {
                                        this.state.user.privilige[0].isAdmin ? 
                                            <td><a className="waves-effect btn-floating red modal-trigger" href={"#"+ticket._id}><i className="material-icons">account_circle</i></a></td>:""
                                        }
                                        <td><a onClick={this.changeState}  className="btn-floating blue"><i id={ticket._id}className="material-icons">check_circle</i></a></td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                <div key="modal1" id="modal1" className="modal">
                            <div className="modal-content">
                                <h4>Nuevo Ticket</h4>
                                    <form>
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <input required="required"
                                                value={this.state.descripcion}
                                                onChange={this.onDescripcionChange}
                                                id="descripcion" name="descripcion" type="text" className="validate"/>
                                                <label htmlFor="descripcion">Descripcion</label>
                                            </div>
                                            {
                                                this.state.user.privilige[0].isAdmin ? 
                                                <div className="input-field col s12">
                                                        <select value={this.state.inquilino} name="inquilinos" onChange={this.onInquilinoChange}>
                                                        <option value="" defaultValue>Seleccione un inquilino</option>
                                                        {   
                                                                this.state.inquilinos.map (inquilino => {
                                                                    return(
                                                                        <option value={inquilino._id}>{inquilino.inquilino.local} - {inquilino.name} - {inquilino.inquilino.empresa.name}</option>
                                                                    )
                                                                })
                                                        }
                                                        </select>
                                                        <label>Inquilinos</label>
                                                </div>
                                                :
                                                <div className="input-field col s12" hidden>
                                                    <input required="required"
                                                    value={this.state.user._id}
                                                    id="descripcion" name="descripcion" type="text" className="validate"/>
                                                </div>
                                            }
                                            
                                            <div className="input-field col s12">
                                                    <select value={this.state.tipo} name="tipos" onChange={this.onSelectChange}>
                                                        <option value="" defaultValue>Tipos</option>
                                                    {   
                                                            this.state.tipos.map (tipo => {
                                                                return(
                                                                    <option value={tipo._id}>{tipo.category.name}-{tipo.name}</option>
                                                                )
                                                            })
                                                    }
                                                    </select>
                                                    <label>Tipos</label>
                                                </div>
                                            
                                                
                                            <div className="col s2">
                                                <button 
                                                    className="btn btn-primary"
                                                    onClick={this.onAddTicket}
                                                >Agregar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                </div>
                
            </div>
        );
    }catch(err){
        console.log(err)
    }
        return(<div></div>); 
    }  
}

const mapStateToProps = (state, props) => ({
    form: state.form
});
const mapDispatchToProps = (dispatch, props) => ({
    startCreateUser: (data) => dispatch(startCreateUser(data)),
    createEmpresaItems: () => createEmpresaItems(),
    changeStatus: (status) => dispatch(changeStatus(status))
});

export default connect(mapStateToProps, mapDispatchToProps)(TicketsPage);
