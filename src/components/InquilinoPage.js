import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import Navbar from './Navbar';
import AuthChecker from '../routers/AuthChecker';
import M from 'materialize-css';
import axios from 'axios';
import { getStorage } from '../helpers/cookie';

export class InquilinosPage extends React.Component{
    constructor(props){       
        super(props);
    }
    state = {
        inquilinos: [],
        empresas: [],
        empresa: "",
        dato: "",
        contactoDescri: "",
        contactoNumero: "",
        inquilino:""
    };
    onSelectChange = (e) => {
        let local = e.target.value;
        this.setState({empresa:local});
    };
    onContactoChange = (e) => {
        let dato = e.target.value;
        this.setState({contactoDescri:dato});
    };
    onNumeroChange = (e) => {
        let dato = e.target.value;
        this.setState({contactoNumero:dato});
    };
    removeNumero = (e) => {
        const reqInstance = axios.create({
            headers: {
                'x-auth': getStorage('usrJwt')
            }
        });
        return reqInstance.post('/admin/RmContacto', {"data":e.target.id}).then((res) => {
            this.update();    
            M.toast({html: res.data})
        }).catch((e) => {
            console.log('error');
        });
    }
    onAddContact = (e) => {
        if(this.state.contactoNumero=="" || this.state.contactoDescri==""){
            M.toast({html: "Formulario no puede estar vacio"})            
        }else{
            e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/admin/Contacto', {"descripcion":this.state.contactoDescri,"numero":this.state.contactoNumero,"id":e.target.id}).then((res) => {
                this.setState({contactoDescri:""});
                this.setState({contactoNumero:""});
                this.update();    
                M.toast({html: res.data})
            }).catch((e) => {
                console.log('error');
            });
        } 
        
       
    }
    getEmpresa = async () => {
        try {
          return await axios.get('/empresa/all')
        } catch (error) {
          console.error(error)
        }
      }

    async selectEmpresa() {
        const query = await this.getEmpresa()
        let empresas = query.data;
        return empresas;         
      }
    getInquilinos = async () => {
        try {
          return await axios.get('/admin/inquilinos')
        } catch (error) {
          console.error(error)
        }
      }
      onDatoChange = (e) => {
        let dato = e.target.value;
        this.setState({dato});
    };
    async selectInquilinos() {
        const query = await this.getInquilinos()
        let inquilinos = query.data;
        return inquilinos;         
      }

    async componentDidMount(){
        await this.update()        
        M.AutoInit();                 
    }

    async update(){
        let cate = await this.selectEmpresa();  
        this.setState({empresas:cate});
        let data = await this.selectInquilinos(); 
        this.setState({inquilinos:data});
        console.log(this.state)
    }

     changeState = async(e) =>{
        try {
            let result = await axios.get('/admin/usuarios/state?id='+e.target.id)
            await this.update()            
          } catch (error) {
            console.error(error)
          }
    }
    onSubmit = (e) => {
        e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/admin/inquilinos', {"dato":this.state.dato,"empresa":this.state.empresa}).then((res) => {
                this.setState({inquilinos:res.data});    
                M.AutoInit();                                 
                
            }).catch((e) => {
                console.log('error');
            });
       
    }
    render(){
        return(
            <div>
            <AuthChecker history={this.props.history} isPublic={false} accessRequire={'isAdmin'}/>
            <Navbar />  
                <div className='container'>
                <h3 className="blue-text">Usuarios inquilinos</h3>
                <form>
                <div class="row">
                    <div class="input-field col s5">
                        <input 
                        value={this.state.dato}
                        onChange={this.onDatoChange}
                        placeholder="Busqueda" id="dato" name="dato" type="text" class="validate"/>
                        <label htmlFor="first_name">Busqueda</label>
                    </div>
                    <div className="input-field col s5">
                            <select name="empresas" onChange={this.onSelectChange}>
                            <option value="" selected>Todas las empresas</option>
                            {   
                                    this.state.empresas.map (cate => {
                                        return(
                                            <option value={cate._id}>{cate.name}</option>
                                        )
                                    })
                            }
                            </select>
                            <label>Empresa</label>
                    </div>
                    <div className="col s2">
                        <button 
                            className="btn btn-primary"
                            onClick={this.onSubmit}
                        >Buscar</button>
                    </div>
                </div>
                </form>
                <table className="centered">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Local</th>
                        <th>Empresa</th>
                        <th>Contactos</th>
                        <th>Cambiar estado</th>
                    </tr>
                    </thead>
                    {
                    this.state.inquilinos.map (inquilino => {
                        return(
                        <div key={inquilino._id} id={inquilino._id} class="modal">
                            <div class="modal-content">
                                <h4>Contactos {inquilino.name}</h4>
                                <form>
                                <div class="row">
                                <form>
                                <div class="row">
                                    <div class="input-field col s5">
                                        <input required="required"
                                        value={this.state.contactoDescri}
                                        onChange={this.onContactoChange}
                                        id="descripcion" name="descripcion" type="text" class="validate"/>
                                        <label htmlFor="descripcion">Descripcion</label>
                                    </div>
                                    <div class="input-field col s5">
                                        <input required="required"
                                        value={this.state.contactoNumero}
                                        onChange={this.onNumeroChange}
                                        id="numero" name="numero" type="text" class="validate"/>
                                        <label htmlFor="numero">Numero</label>
                                    </div>
                                    <div className="col s2">
                                        <button 
                                            id={inquilino._id}
                                            className="btn btn-primary"
                                            onClick={this.onAddContact}
                                        >Agregar</button>
                                    </div>
                                </div>
                                </form>
                                </div>
                                </form>
                                <table className="centered">
                                    <thead>
                                        <tr>
                                            <th>Descripcion</th>
                                            <th>Numero</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {inquilino.contactos.map(contacto =>{
                                            return(
                                                <tr key={contacto.numero}>
                                                    <td>{contacto.descripcion}</td>
                                                    <td>{contacto.numero}</td>
                                                    <td>
                                                    <a onClick={this.removeNumero} className="btn-floating red"><i id={contacto.numero+"|"+inquilino._id+"|"+contacto.descripcion+"|"+contacto._id} className="material-icons">close</i></a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        )
                    })
                }
                    <tbody>
                        {
                            this.state.inquilinos.map (inquilino => {
                                return(
                                    <tr key={inquilino._id}>
                                        <td>{inquilino.name}</td>
                                        <td>{inquilino.inquilino.local}</td>
                                        <td>{inquilino.inquilino.empresa.name}</td>
                                        <td><a className="waves-effect btn-floating red modal-trigger" href={"#"+inquilino._id}><i className="material-icons">account_circle</i></a></td>
                                        <td>{inquilino.privilige[0].isActivate ? 
                                            <a onClick={this.changeState}  className="btn-floating blue"><i id={inquilino._id}className="material-icons">check_circle</i></a>: <a onClick={this.changeState}  className="btn-floating red"><i id={inquilino._id}className="material-icons">close</i></a>}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    form: state.form
});
const mapDispatchToProps = (dispatch, props) => ({
    startCreateUser: (data) => dispatch(startCreateUser(data)),
    createEmpresaItems: () => createEmpresaItems(),
    changeStatus: (status) => dispatch(changeStatus(status))
});

export default connect(mapStateToProps, mapDispatchToProps)(InquilinosPage);
