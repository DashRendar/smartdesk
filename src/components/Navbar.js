import React from 'react';
import { Link } from 'react-router-dom';
import LogoutBtn from './utils/LogoutBtn';
import CreateBtn from './utils/CreateBtn';
import AdminTicketBtn from './utils/AdminTicketBtn';
import Empresas from './utils/Empresas';
import Estadistica from './utils/Estadistica';
import Usuarios from './utils/Usuarios';
import Reportes from './utils/Reportes';
import Regresion from './utils/Regresion';
import Categorias from './utils/Categorias';
import UsuariosList from './utils/UsuariosList';
import M from 'materialize-css';


export const Navbar = () => (
<div>
<ul id="dropdown1" className="dropdown-content">
  <li><a href="/tecnicos">Tecnicos</a></li>
  <li><a href="/inquilinos">Inquilinos</a></li>
  <li><a href="/administradores">Administradores</a></li>
  <li className="divider" tabIndex="-1"></li>
  <li><CreateBtn /></li>
</ul>
<ul id="dropdown2" className="dropdown-content">
  <li><a href="/tiquetes">Tickets Abiertos</a></li>
  <li><a href="/tiquetes/cerrados">Tickets Cerrados</a></li>
  <AdminTicketBtn />
</ul>
<ul id="dropdown3" className="dropdown-content">
    <li><Estadistica/></li>
    <li><Regresion/></li>
</ul>
<nav>
  <div className="nav-wrapper blue darken-2">
    <a href="/" className="brand-logo" style={{paddingLeft: '54px'}} >Smart Desk</a>
    <a href="#" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></a>
    <ul className="right hide-on-med-and-down">
    <li><Reportes/></li>
    <li><a className="dropdown-trigger" href="#!" data-target="dropdown2">Tickets<i className="material-icons right">arrow_drop_down</i></a></li>
    <li><Usuarios/></li>
    <li><Empresas/></li>
    <li><Categorias/></li>
    <li><a href="/perfil">Perfil</a></li>
    <li><LogoutBtn /></li>
    </ul>
  </div>
</nav>
<ul className="sidenav" id="mobile-demo">  
    <li><Regresion/></li>
  <li><Estadistica/></li>
  <li className="divider" tabIndex="-1"></li>
  <li><a href="/tiquetes">Tickets Abiertos</a></li>
  <li><a href="/tiquetes/cerrados">Tickets Cerrados</a></li>
  <li><AdminTicketBtn /></li>
  <li className="divider" tabIndex="-1"></li>
  <li><UsuariosList /></li>
  <li><a href="/perfil">Perfil</a></li>
    <li><Empresas/></li>
    <li><Categorias/></li>
  <li><LogoutBtn /></li>
</ul>

  <div hidden>
  {setTimeout(function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems, {constrainWidth:false});
    }
    ,500)}
  </div>
  </div>

);



export default Navbar;
