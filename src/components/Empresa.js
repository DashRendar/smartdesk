import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import Navbar from './Navbar';
import AuthChecker from '../routers/AuthChecker';
import M from 'materialize-css';
import axios from 'axios';
import { getStorage } from '../helpers/cookie';

export class Empresas extends React.Component{
    constructor(props){       
        super(props);
    }
    state = {
        empresas: [],
        nombre: "",
        descripcion: "",
        find:""
    };
    onNombreChange = (e) => {
        let dato = e.target.value;
        this.setState({nombre:dato});
    };
    onSubmit = (e) => {
        e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/empresa/find', {"dato":this.state.find}).then((res) => {
                this.setState({empresas:res.data});    
            }).catch((e) => {
                console.log('error');
            });
       
    }
    onFindChange = (e) => {
        let dato = e.target.value;
        this.setState({find:dato});
    };
    onDescripcionChange = (e) => {
        let dato = e.target.value;
        this.setState({descripcion:dato});
    };
    onAddEmpresa = (e) => {
        if(this.state.descripcion=="" || this.state.nombre==""){
            M.toast({html: "Formulario no puede estar vacio"})            
        }else{
            e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/empresa/add', {"descripcion":this.state.descripcion,"nombre":this.state.nombre}).then((res) => {
                this.setState({nombre:""});
                this.setState({descripcion:""});
                this.update();    
                M.toast({html: res.data})
            }).catch((e) => {
                console.log('error');
            });
        } 
        
       
    }
    getEmpresas = async () => {
        try {
          return await axios.get('/empresa/all')
        } catch (error) {
          console.error(error)
        }
      }
      onDatoChange = (e) => {
        let dato = e.target.value;
        this.setState({dato});
    };
    async selectEmpresas() {
        const query = await this.getEmpresas()
        let empresas = query.data;
        return empresas;         
      }

    async componentDidMount(){
        await this.update()        
        M.AutoInit();                 
    }

    async update(){
        let data = await this.selectEmpresas(); 
        this.setState({empresas:data});
    }

     changeState = async(e) =>{
        try {
            let result = await axios.get('/empresa/state?id='+e.target.id)
            await this.update()            
          } catch (error) {
            console.error(error)
          }
    }
    render(){
        return(
            <div>
            <AuthChecker history={this.props.history} isPublic={false} accessRequire={'isAdmin'}/>
            <Navbar />  
                <div className='container'>
                <h3 className="blue-text">Empresas registradas</h3>
                <form>
                <div class="row">
                    <div class="input-field col s5">
                        <input 
                        value={this.state.find}
                        onChange={this.onFindChange}
                        placeholder="Busqueda" id="find" name="find" type="text" class="validate"/>
                        <label htmlFor="first_name">Busqueda</label>
                    </div>
                    <div className="col s2">
                        <button 
                            className="btn btn-primary"
                            onClick={this.onSubmit}
                        >Buscar</button>
                    </div>
                </div>
                </form>
                <div className="row">
                    <div className="col s2 push-s10">
                        <a class="btn-floating yellow modal-trigger" href="#modal1"><i className="material-icons">add</i></a>

                    <div id="modal1" class="modal">
                        <div class="modal-content">
                            <h4>Agregar Empresa</h4>
                            <form>
                                <div class="row">
                                    <div class="input-field col s5">
                                        <input required="required"
                                        value={this.state.nombre}
                                        onChange={this.onNombreChange}
                                        id="nombre" name="nombre" type="text" class="validate"/>
                                        <label htmlFor="nombre">Nombre</label>
                                    </div>
                                    <div class="input-field col s5">
                                        <input required="required"
                                        value={this.state.descripcion}
                                        onChange={this.onDescripcionChange}
                                        id="descripcion" name="descripcion" type="text" class="validate"/>
                                        <label htmlFor="descripcion">Descripcion</label>
                                    </div>
                                    <div className="col s2">
                                        <button 
                                            className="btn btn-primary"
                                            onClick={this.onAddEmpresa}
                                        >Agregar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
                <table className="centered">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Cambiar estado</th>
                    </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.empresas.map (empresa => {
                                return(
                                    <tr key={empresa._id}>
                                        <td>{empresa.name}</td>
                                        <td>{empresa.description}</td>
                                        <td>{empresa.isDeleted ? 
                                            <a onClick={this.changeState}  className="btn-floating red"><i id={empresa._id}className="material-icons">close</i></a>:<a onClick={this.changeState}  className="btn-floating blue"><i id={empresa._id}className="material-icons">check_circle</i></a>}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    form: state.form
});
const mapDispatchToProps = (dispatch, props) => ({
    changeStatus: (status) => dispatch(changeStatus(status))
});

export default connect(mapStateToProps, mapDispatchToProps)(Empresas);
