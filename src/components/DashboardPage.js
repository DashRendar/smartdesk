import React from 'react';
import Navbar from './Navbar';
import LogoutBtn from './utils/LogoutBtn';
import AuthChecker from '../routers/AuthChecker';
import Ticket from './Tickets';

const DashboardPage = (props) => (
    <div>
    <Navbar />    
    <div className="container">
        <AuthChecker history={props.history} isPublic={false} accessRequire={'isActivate'}/>
        <h3>Tickets</h3>
    </div>
    </div>
);

export default DashboardPage;
