import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import Navbar from './Navbar';
import AuthChecker from '../routers/AuthChecker';
import M from 'materialize-css';
import axios from 'axios';
import { getStorage } from '../helpers/cookie';

export class TecnicosPage extends React.Component{
    constructor(props){       
        super(props);
    }
    state = {
        tecnicos: [],
        categorias: [],
        categoria: "",
        dato: "",
        contactoDescri: "",
        contactoNumero: "",
        tecnico:""
    };
    onSelectChange = (e) => {
        let cargo = e.target.value;
        this.setState({categoria:cargo});
    };
    onContactoChange = (e) => {
        let dato = e.target.value;
        this.setState({contactoDescri:dato});
    };
    onNumeroChange = (e) => {
        let dato = e.target.value;
        this.setState({contactoNumero:dato});
    };
    removeNumero = (e) => {
        const reqInstance = axios.create({
            headers: {
                'x-auth': getStorage('usrJwt')
            }
        });
        return reqInstance.post('/admin/RmContacto', {"data":e.target.id}).then((res) => {
            this.update();    
            M.toast({html: res.data})
        }).catch((e) => {
            console.log('error');
        });
    }
    onAddContact = (e) => {
        if(this.state.contactoNumero=="" || this.state.contactoDescri==""){
            M.toast({html: "Formulario no puede estar vacio"})            
        }else{
            e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/admin/Contacto', {"descripcion":this.state.contactoDescri,"numero":this.state.contactoNumero,"id":e.target.id}).then((res) => {
                this.setState({contactoDescri:""});
                this.setState({contactoNumero:""});
                this.update();    
                M.toast({html: res.data})
            }).catch((e) => {
                console.log('error');
            });
        } 
        
       
    }
    getCategoria = async () => {
        try {
          return await axios.get('/categoria/all')
        } catch (error) {
          console.error(error)
        }
      }

    async selectCategoria() {
        const query = await this.getCategoria()
        let categorias = query.data;
        return categorias;         
      }
    getTecnicos = async () => {
        try {
          return await axios.get('/admin/tecnicos')
        } catch (error) {
          console.error(error)
        }
      }
      onDatoChange = (e) => {
        let dato = e.target.value;
        this.setState({dato});
    };
    async selectTecnicos() {
        const query = await this.getTecnicos()
        let tecnicos = query.data;
        return tecnicos;         
      }

    async componentDidMount(){
        await this.update()        
        M.AutoInit();                 
    }

    async update(){
        let cate = await this.selectCategoria();  
        this.setState({categorias:cate});
        let data = await this.selectTecnicos(); 
        this.setState({tecnicos:data});
        console.log(this.state)
    }

     changeState = async(e) =>{
        try {
            let result = await axios.get('/admin/usuarios/state?id='+e.target.id)
            await this.update()            
          } catch (error) {
            console.error(error)
          }
    }
    onSubmit = (e) => {
        e.preventDefault();
            const reqInstance = axios.create({
                headers: {
                    'x-auth': getStorage('usrJwt')
                }
            });
            return reqInstance.post('/admin/tecnicos', {"dato":this.state.dato,"cats":this.state.categoria}).then((res) => {
                this.setState({tecnicos:res.data});    
                M.AutoInit();                                 
                
            }).catch((e) => {
                console.log('error');
            });
       
    }
    render(){
        return(
            <div>
            <AuthChecker history={this.props.history} isPublic={false} accessRequire={'isAdmin'}/>
            <Navbar />  
                <div className='container'>
                <h3 className="blue-text">Usuarios Tecnicos</h3>
                <form>
                <div class="row">
                    <div class="input-field col s5">
                        <input 
                        value={this.state.dato}
                        onChange={this.onDatoChange}
                        placeholder="Busqueda" id="dato" name="dato" type="text" class="validate"/>
                        <label htmlFor="first_name">Busqueda</label>
                    </div>
                    <div className="input-field col s5">
                            <select name="categorias" onChange={this.onSelectChange}>
                            <option value="" selected>Todas las categorias</option>
                            {   
                                    this.state.categorias.map (cate => {
                                        return(
                                            <option value={cate._id}>{cate.name}</option>
                                        )
                                    })
                            }
                            </select>
                            <label>Categoria</label>
                    </div>
                    <div className="col s2">
                        <button 
                            className="btn btn-primary"
                            onClick={this.onSubmit}
                        >Buscar</button>
                    </div>
                </div>
                </form>
                <table className="centered">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Cargo</th>
                        <th>Categoria</th>
                        <th>Contactos</th>
                        <th>Cambiar estado</th>
                    </tr>
                    </thead>
                    {
                    this.state.tecnicos.map (tecnico => {
                        return(
                        <div key={tecnico._id} id={tecnico._id} class="modal">
                            <div class="modal-content">
                                <h4>Contactos {tecnico.name}</h4>
                                <form>
                                <div class="row">
                                <form>
                                <div class="row">
                                    <div class="input-field col s5">
                                        <input required="required"
                                        value={this.state.contactoDescri}
                                        onChange={this.onContactoChange}
                                        id="descripcion" name="descripcion" type="text" class="validate"/>
                                        <label htmlFor="descripcion">Descripcion</label>
                                    </div>
                                    <div class="input-field col s5">
                                        <input required="required"
                                        value={this.state.contactoNumero}
                                        onChange={this.onNumeroChange}
                                        id="numero" name="numero" type="text" class="validate"/>
                                        <label htmlFor="numero">Numero</label>
                                    </div>
                                    <div className="col s2">
                                        <button 
                                            id={tecnico._id}
                                            className="btn btn-primary"
                                            onClick={this.onAddContact}
                                        >Agregar</button>
                                    </div>
                                </div>
                                </form>
                                </div>
                                </form>
                                <table className="centered">
                                    <thead>
                                        <tr>
                                            <th>Descripcion</th>
                                            <th>Numero</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {tecnico.contactos.map(contacto =>{
                                            return(
                                                <tr key={contacto.numero}>
                                                    <td>{contacto.descripcion}</td>
                                                    <td>{contacto.numero}</td>
                                                    <td>
                                                    <a onClick={this.removeNumero} className="btn-floating red"><i id={contacto.numero+"|"+tecnico._id+"|"+contacto.descripcion+"|"+contacto._id} className="material-icons">close</i></a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        )
                    })
                }
                    <tbody>
                        {
                            this.state.tecnicos.map (tecnico => {
                                return(
                                    <tr key={tecnico._id}>
                                        <td>{tecnico.name}</td>
                                        <td>{tecnico.tecnico.cargo}</td>
                                        <td>{tecnico.tecnico.categoria.name}</td>
                                        <td><a className="waves-effect btn-floating red modal-trigger" href={"#"+tecnico._id}><i className="material-icons">account_circle</i></a></td>
                                        <td>{tecnico.privilige[0].isActivate ? 
                                            <a onClick={this.changeState}  className="btn-floating blue"><i id={tecnico._id}className="material-icons">check_circle</i></a>: <a onClick={this.changeState}  className="btn-floating red"><i id={tecnico._id}className="material-icons">close</i></a>}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    form: state.form
});
const mapDispatchToProps = (dispatch, props) => ({
    startCreateUser: (data) => dispatch(startCreateUser(data)),
    createEmpresaItems: () => createEmpresaItems(),
    changeStatus: (status) => dispatch(changeStatus(status))
});

export default connect(mapStateToProps, mapDispatchToProps)(TecnicosPage);
