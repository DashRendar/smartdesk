@app.route('/tecnico', methods=['POST'])
def predice_tecnico_tipo():
    tickets = mongo.db.tickets.find({})
    x = np.array([1,1])
    y = np.array([])
    data = request.get_json()
    tecnico = floar(data["tecnico"])
    tipo = floar(data["tipo"])
    for ticket in tickets:
        Xa = np.array([[ticket["tecnico_uid"],ticket["tipo_uid"]]])
        X = np.concatenate((X, Xa), axis = 0)
        y = np.append(y, ticket["tiempo_cierre"])
    X=np.delete(X,0,0)
    reg = LinearRegresion().fit(X, y)
    prediccion = reg.predict(np.array([[tecnico, tipo]]))
    return jsonify({"prediccion:"prediccion[0]}) 